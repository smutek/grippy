<?php
/**
 * Sage includes
 *
 * The $sage_includes array determines the code library included in your theme.
 * Add or remove files to the array as needed. Supports child theme overrides.
 *
 * Please note that missing files will produce a fatal error.
 *
 * @link https://github.com/roots/sage/pull/1042
 */
$sage_includes = [
  'lib/assets.php',    // Scripts and stylesheets
  'lib/extras.php',    // Custom functions
  'lib/setup.php',     // Theme setup
  'lib/titles.php',    // Page titles
  'lib/wrapper.php',   // Theme wrapper class
  'lib/customizer.php', // Theme customizer
  'lib/acf-gforms.php', // ACF / Gravity Forms
  'lib/yamm-nav-walker.php', // Bootstrap Nav Walker
  'lib/controllers.php', // Controllers
  'lib/product-tabs-controller.php', // Controllers
  'lib/Modals.php', // Modal class uppercase
  'lib/Heroes.php', // Hero class
  'lib/mobileDetect.php', // Mobile detection library
  'lib/custom-types/press.php', // Press Links CPT
  'lib/custom-types/products.php', // Products CPT
  'lib/custom-types/retailers.php', // Retailers CPT
  'lib/custom-types/productCategories.php', // Product Categories (taxonomy)
  'lib/custom-types/sizesAvailable.php', // Sizes Available (taxonomy)
  'lib/custom-types/colors.php', // Colors Available (taxonomy)
  'lib/custom-types/benefits.php' // Benefits & Features (taxonomy)
];

foreach ($sage_includes as $file) {
  if (!$filepath = locate_template($file)) {
    trigger_error(sprintf(__('Error locating %s for inclusion', 'sage'), $file), E_USER_ERROR);
  }

  require_once $filepath;
}
unset($file, $filepath);
