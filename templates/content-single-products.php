<?php
while ( have_posts() ) : the_post();

$review = \Roots\Sage\Controllers\powerReviews();

?>

  <article <?php post_class(); ?>>
    <header class="jumbotron">
      <div class="container">
        <div class="row">

          <?php //get_template_part( 'templates/modules/breadcrumbs' ); ?>

          <?php if ( have_rows( 'product_media' ) ) : // product slider ?>
            <div class="col-sm-6 product-media">
              <?php include (locate_template('templates/sections/product-slider.php')); ?>
            </div>
          <?php endif; ?>

          <div class="col-sm-6 product-branding">
            <?php include locate_template('templates/sections/product-branding.php'); ?>
          </div>

        </div>
      </div>
    </header>

    <div class="container">
      <div class="entry-content">
        <?php include (locate_template('templates/sections/product-tabs.php')); ?>
      </div>
    </div>
  </article>
<?php endwhile; ?>
