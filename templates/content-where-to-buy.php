<?php
use Roots\Sage\Controllers;

if ( have_rows( 'wtb_add_categories' ) ) :
  $categories = Controllers\where_to_buy( 'wtb_add_categories', 'wtb_add_category' );
  ?>
  <div class="wtb-nav-wrap">
    <div class="container">
      <header class="wtb-nav-header">
        <h2>Select Product Category</h2>
        <?php if ( get_the_content() !== "" ) : ?>
          <div class="header-content">
            <?php the_content(); ?>
          </div>
        <?php endif; ?>
      </header>
      <ul id="wtb-list" class="wtb-category-list list-unstyled list-inline button-group" data-toggle="buttons">
        <?php $count = 0;
        foreach ( $categories as $category ) : $count ++; ?>
          <li class="wtb-list-item <?= $category['classes']; ?>">
            <a class="wtb-category-button collapsed" role="button" data-toggle="collapse" data-parent="#wtb-list" href="#<?= $category['slug']; ?>"
               aria-expanded="false" aria-controls="<?= $category['slug']; ?>">
              <div class="thumbnail-image">
                <img class="img-responsive" src="<?= $category['image_url']; ?>" alt="<?= $category['image_alt']; ?>">
              </div>
              <h3 class="wtb-category-title"><?= $category['title']; ?></h3>
            </a>
          </li>
        <?php endforeach; ?>
      </ul>
    </div>
  </div>
  <div class="wtb-lower">
    <ul class="list list-unstyled product-list">
      <?php $count = 0;
      foreach ( $categories as $category ) : $count ++; ?>
        <span class="sr-only"><?= $category['title']; ?></span>
        <li class="product collapse" id="<?= $category['slug']; ?>" aria-expanded="false" data-collapse-group="product-list">
          <?php include( locate_template( 'templates/sections/accordion-where-to-buy.php' ) ); ?>
        </li>
      <?php endforeach; ?>
    </ul>
  </div>
<?php endif; ?>
