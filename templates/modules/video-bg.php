<div class="video-wrap" role="presentation">
  <div class="video-container">
    <video class="video" autoplay="autoplay" loop="loop" muted="">
      <source src="<?= $hero['videoBG']; ?>" type="video/mp4">
    </video>
  </div>
</div>
