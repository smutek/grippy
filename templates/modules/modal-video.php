<!-- Video Modal: <?= $this->modalID; ?> -->
<div class="modal video-modal fade" id="<?= $this->modalID; ?>" tabindex="-1" role="dialog" aria-labelledby="<?=$this->modalID; ?>Label">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body video-wrap">
        <?= $this->video; ?>
      </div>
    </div>
  </div>
</div>
