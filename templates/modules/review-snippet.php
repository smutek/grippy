<?php if(get_field('reviews_show_tab')) : ?>
<div class="pr_review_summary">
  <script type="text/javascript">
    POWERREVIEWS.display.snippet(document, {
        pr_page_id: '<?= $review['page_id']; ?>',
        pr_read_review: false
      }
    );
  </script>
</div>
<?php endif; ?>
