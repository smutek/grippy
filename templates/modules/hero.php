<?php $hero = $this->heroContent(); ?>

<section
  class="hero jumbotron"<?php if ( $hero['bg'] ): ?> style="background-image: url(<?= $hero['bg']; ?>);"<?php endif; ?>>
  <?php if ( $hero['videoBG'] ) : ?>
    <?php include(locate_template( 'templates/modules/video-bg.php' )); ?>
  <?php endif; ?>
  <div class="container">
    <div class="hero-inner">
      <div class="hero-content">
        <h2<?php if ( $hero['textBG'] ) {
          echo ' class="sr-only" ';
        } ?>><?= $hero['heading']; ?></h2>
        <?php if ( $hero['textBG'] ) : ?>
          <img class="img-responsive" src="<?= $hero['textBG']; ?>" alt="<?= $hero['heading']; ?>">
        <?php endif; ?>
        <div class="hero-copy">
          <?= $hero['copy']; ?>
        </div>
        <?php if($hero['button']) : ?>
          <footer>
            <?= $hero['button']; ?>
          </footer>
        <?php endif; ?>
      </div>
    </div>
  </div>
</section>

