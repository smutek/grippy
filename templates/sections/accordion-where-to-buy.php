<ul class="panel-group product-panel-group list-unstyled" id="<?= $category['slug']; ?>-accordion" role="tablist"
    aria-multiselectable="true">
  <?php foreach ( $category['products'] as $product ) : ?>
    <li class="product-panel">
      <div class="panel-heading" role="tab" id="<?= $product['slug']; ?>-title">
        <h4 class="panel-title container">
          <a role="button" data-toggle="collapse" data-parent="#<?= $category['slug']; ?>-accordion"
             href="#<?= $product['slug']; ?>" aria-expanded="false"
             aria-controls="collapse<?= $product['slug']; ?>" class="panel-toggle collapsed">
            <?= $product['title']; ?>
          </a>
        </h4>
      </div>
      <div id="<?= $product['slug']; ?>" class="panel-collapse collapse" role="tabpanel"
           aria-labelledby="<?= $product['slug']; ?>-title">
        <div class="panel-body">
          <p><strong>Select Size</strong></stro></p>
          <?php include( locate_template( 'templates/sections/tabs-where-to-buy.php' ) ); ?>
          <footer class="panel-footer">
            <a role="button" data-toggle="collapse" data-parent="#<?= $category['slug']; ?>-accordion"
               href="#<?= $product['slug']; ?>" aria-expanded="false"
               aria-controls="<?= $product['slug']; ?>" class="panel-footer-toggle">close</a>
          </footer>
        </div>
      </div>
    </li>
  <?php endforeach; ?>
</ul>
