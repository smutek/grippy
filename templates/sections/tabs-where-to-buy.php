<ul class="wtb-sku-tabs list-unstyled list-inline container" role="tablist">
  <?php
  $count = 1;
  foreach ( $product['skus'] as $sku ) : ?>
    <li role="presentation" class="sku-nav-tab">
      <a href="#tab-<?= $sku['number']; ?>" aria-controls="tab-<?= $sku['number']; ?>" role="tab"
         data-toggle="tab">
        <div class="thumbnail-image">
          <img class="img-responsive" src="<?= $sku['single_img_url']; ?>" alt="<?= $sku['single_img_alt']; ?>">
        </div>
        <div class="sku-info">
          <p>
            <?= $sku['size']; ?>
            <span class="sr-only">SKU Number: <?= $sku['number']; ?></span>
          </p>
        </div>
      </a>
    </li>
    <?php
    $count ++;
  endforeach; ?>
  <!-- Nav tabs -->
</ul>

<!-- Tab panes -->
<div class="tab-content">
  <?php
  $count = 1;
  foreach ( $product['skus'] as $sku ) : ?>
    <div role="tabpanel" class="tab-pane sku-tab-pane" id="tab-<?= $sku['number']; ?>">
      <ul class="sku-retailer-list list-unstyled list-inline container">
        <?php foreach ( $sku['retailers'] as $retailer ) : ?>
          <li class="sku-retailer">
            <a href="<?= $retailer['url']; ?>" target="_blank">
              <img class="img-responsive" src="<?= $retailer['logo_url']; ?>" alt="<?= $retailer['logo_alt']; ?>">
              <span class="sr-only"><?= $retailer['name']; ?></span>
              <span class="buy">Buy Online</span>
            </a>
          </li>
        <?php endforeach; ?>
      </ul>

    </div>
    <?php
    $count ++;
  endforeach; ?>
</div>
