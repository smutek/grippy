<?php
use Roots\Sage\Controllers;

$branding = Controllers\productBranding();
?>
<header class="product-header" style="background: <?= $branding['categoryColor']; ?>">
  <h1 class="entry-title"><?php the_title(); ?></h1>
</header>

<?php if(get_field('reviews_show_tab')) : ?>
  <div class="reviews">
    <?php include (locate_template('templates/modules/review-snippet.php')); ?>
  </div>
<?php endif; ?>
<ul class="list-unstyled product-benefits">
  <?php foreach ( $branding['benefits'] as $benefit ) : ?>
    <li>
      <img class="img-responsive" src="<?= $benefit['iconURL']; ?>" alt="<?= $benefit['iconALT']; ?>">
      <p><?= $benefit['name']; ?></p>
    </li>
  <?php endforeach; ?>
</ul>

