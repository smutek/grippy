<?php
use Roots\Sage\ProductTabsController;
$info = ProductTabsController\productInfo();
?>
<!-- Nav tabs -->
<ul class="nav product-nav-tabs nav-tabs" role="tablist">
  <?php $count = 1;
  foreach ( $info['navTabs'] as $navTab ) : ?>
    <li role="presentation" class="product-nav-tab<?php if ( $count === 1 ) {echo ' active';} ?>">
      <?= $navTab; ?>
    </li>
    <?php $count ++; endforeach; ?>
</ul>

<!-- Tab panes -->
<div class="product-tab-content tab-content">
  <?php if ( $info['overview_show_tab'] ) : ?>
    <div role="tabpanel" class="tab-pane active" id="<?= $info['overview_tab_id']; ?>">
      <?php include( locate_template( 'templates/product-tabs/product-overview.php' ) ); ?>
    </div>
  <?php endif; ?>
  <?php if ( $info['specs_show_tab'] ) : ?>
    <div role="tabpanel" class="tab-pane" id="<?= $info['specs_tab_id']; ?>">
      <?php include( locate_template( 'templates/product-tabs/product-specs.php' ) ); ?>
    </div>
  <?php endif; ?>
  <?php if ( $info['wtb_show_tab'] ) : ?>
    <div role="tabpanel" class="tab-pane wtb-tabs" id="<?= $info['wtb_tab_id']; ?>">
      <?php include( locate_template( 'templates/product-tabs/product-where-to-buy.php' ) ); ?>
    </div>
  <?php endif; ?>
  <?php if ( $info['tips_show_tab'] ) : ?>
    <div role="tabpanel" class="tab-pane" id="<?= $info['tips_tab_id']; ?>">
      <?php include( locate_template( 'templates/product-tabs/product-where-to-buy.php' ) ); ?>
    </div>
  <?php endif; ?>
  <?php if ( $info['skus_show_tab'] ) : ?>
    <div role="tabpanel" class="tab-pane" id="<?= $info['skus_tab_id']; ?>">
      <?php include( locate_template( 'templates/product-tabs/product-sku-details.php' ) ); ?>
    </div>
  <?php endif; ?>
  <?php if ( $info['sds_show_tab'] ) : ?>
    <div role="tabpanel" class="tab-pane" id="<?= $info['sds_tab_id']; ?>">
      <?php include( locate_template( 'templates/product-tabs/product-sds-tdb.php' ) ); ?>
    </div>
  <?php endif; ?>
  <?php if ( $info['reviews_show_tab'] ) : ?>
    <div role="tabpanel" class="tab-pane" id="<?= $info['reviews_tab_id']; ?>">
      <?php include( locate_template( 'templates/product-tabs/product-reviews.php' ) ); ?>
    </div>
  <?php endif; ?>
  <?php if ( $info['additional_tabs'] !== false ) :
    foreach ( $info['additional_tabs'] as $additional_tab ) : ?>
      <div role="tabpanel" class="tab-pane" id="<?= $additional_tab['id']; ?>">
        <?= $additional_tab['content']; ?>
      </div>
    <?php endforeach; endif; ?>
</div>
