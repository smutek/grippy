<?php
if ( have_rows( 'home_add_categories' ) ) :
  $heading = get_field( 'home_products_heading' );
  $button = get_field( 'home_products_button_text' );
  $bg = get_field( 'home_products_bg' );
  ?>

  <section class="home-section home-products"<?php if($bg): ?> style="background: #000 url(<?= $bg; ?>) no-repeat;" <?php endif; ?>>
    <div class="container">
      <h2 class="sr-only"><?= $heading; ?></h2>

      <div class="row home-products-row">
        <?php while ( have_rows( 'home_add_categories' ) ) : the_row();

        $termID = get_sub_field( 'home_add_category' );
        $termObject = get_term_by('ID', $termID, 'product_categories');
        $termName = $termObject->name;
        $termLink = '/' . $termObject->slug;
        $termImage =  get_field('product_category_image', 'term_' . $termID);
        ?>
          <div class="col-xs-6 col-sm-3 home-product">
            <div class="thumbnail-image">
              <img class="img-responsive" src="<?= $termImage['url']; ?>">
            </div>
            <h3><?= $termName; ?></h3>
            <footer>
              <p><a href="<?= $termLink; ?>" class="btn btn-default" role="button"><?= $button; ?></a></p>
            </footer>

          </div>
        <?php endwhile; // end while have rows
        ?>
      </div>
    </div>
  </section>

<?php endif; // end if have rows
