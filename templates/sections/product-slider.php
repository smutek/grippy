<?php
use Roots\Sage\Controllers;

$slides = Controllers\productSlider( 'product_media' );
?>

<!-- Slider -->
<ul class="product-slider list-unstyled">
  <?php foreach ( $slides as $slide ) : ?>
    <li id="<?= $slide['id']; ?>">
      <div class="thumbnail-image">
        <img class="img-responsive" src="<?= $slide['url']; ?>" alt="<?= $slide['alt']; ?>">
      </div>
    </li>
  <?php endforeach; ?>
</ul>

<!-- Slider Thumbs -->
<ul class="product-slider-thumbnails list-unstyled row">
  <?php foreach ( $slides as $slide ) : ?>
    <li class="col-sm-3<?= $slide['class']; ?>" id="<?= $slide['thumb-id']; ?>"<?= $slide['data']; ?>>
      <img class="img-responsive" src="<?= $slide['thumb']; ?>" alt="Thumbnail for <?= $slide['id']; ?>">
    </li>
  <?php endforeach; ?>
</ul>

