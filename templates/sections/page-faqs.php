<?php
$panel_groups = $section['panel_groups'];

foreach ($panel_groups as $panel_group) :
  if($panel_group['panels']) :
?>
<section class="faqs">

  <h4 class="faq-panel-group-title"><?= $panel_group['title']; ?></h4>

  <div class="panel-group faq-panel-group" id="accordion" role="tablist" aria-multiselectable="true">
    <?php foreach ( $panel_group['panels'] as $faq ) : ?>
      <div class="panel faq-panel">
        <div class="panel-heading" role="tab" id="<?= $faq['ID']; ?>">
          <h5 class="panel-title">
            <a role="button" class="panel-button collapsed" data-toggle="collapse" data-parent="#accordion" href="#<?= $faq['collapseID']; ?>" aria-expanded="true"
               aria-controls="<?= $faq['collapseID']; ?>">
              <?= $faq['heading']; ?>
            </a>
          </h5>
        </div>
        <div id="<?= $faq['collapseID']; ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="<?= $faq['ID']; ?>">
          <div class="panel-body">
            <?= $faq['content']; ?>
          </div>
        </div>
      </div>
    <?php endforeach; ?>

  </div>
<?php
  endif;
  endforeach;
