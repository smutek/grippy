<?php
use Roots\Sage\Controllers;

if ( have_rows( 'tdb_add_categories' ) ):
  $categories = Controllers\technicalData();
  ?>
  <section class="data-sheets">

    <?php foreach ( $categories as $category ) : ?>
      <header class="product-header" style="background: <?= $category['category_color']; ?>">
        <h2><?= $category['name']; ?></h2>
      </header>
      <ul class="list-unstyled product-list">
        <?php foreach ( $category['products'] as $product ) : ?>
          <li class="product-list-item">
            <h3><a href="<?= $product['link']; ?>"><?= $product['product_title']; ?></a></h3>
            <p class="sku-list"><?= $product['sku_list']; ?></p>
            <ul class="data-sheet-list sds-list list-unstyled">
              <?php foreach ( $product['data_sheets'] as $data_sheet ) : ?>
                <?php if ( $data_sheet ) : ?>
                  <li><a href="<?= $data_sheet['file']; ?>" target="_blank"><strong>SDS</strong>
                      (<?= $data_sheet['title']; ?>) <i class="fa fa-file-pdf-o" aria-hidden="true"></i> </a></li>
                <?php endif; ?>
              <?php endforeach; ?>
            </ul>
            <ul class="data-sheet-list tdb-list list-unstyled">
              <?php foreach ( $product['data_bulletins'] as $data_bulletin ) : ?>
                <li><a href="<?= $data_bulletin['file']; ?>" target="_blank"><strong>TDB</strong>
                    (<?= $data_bulletin['title']; ?>) <i class="fa fa-file-pdf-o" aria-hidden="true"></i></a></li>
              <?php endforeach; ?>
            </ul>
          </li>
        <?php endforeach; ?>
      </ul>
    <?php endforeach; ?>
  </section>
<?php endif;
