<?php
use Roots\Sage\Controllers;

if ( have_rows( 'home_media_blocks' ) ) :

  $heading = get_field( 'home_media_title' );
  $pageID = get_the_ID();
  $gridItems = Controllers\mediaGrid( 'home_media_blocks', $pageID );

  ?>

  <section class="home-section home-media-grid">
    <div class="container">
      <h2 class="sr-only"><?= $heading; ?></h2>
      <div class="row media-grid-row">
        <?php foreach ( $gridItems as $gridItem ) :
          ?>
          <div
            class="<?= $gridItem['classes']; ?>">
            <?php if ( $gridItem['bg'] ) : ?>
              <span class="background-helper" role="presentation" style="background: url(<?= $gridItem['bg']; ?>) no-repeat;"></span>
            <?php endif; ?>
            <div class="media-grid-item-content">
              <header>
                <h3><?= $gridItem['title']; ?></h3>
              </header>
              <div class="item-copy">
                <?= $gridItem['copy']; ?>
              </div>
              <footer>
                <?= $gridItem['button']; ?>
              </footer>
            </div>
          </div>
        <?php endforeach; ?>
      </div>
    </div>
  </section>
<?php endif; // end if have rows
