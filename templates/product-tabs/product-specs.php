<div class="row">
  <div class="col-sm-4">

    <section class="product-specs recommended-use">
      <h3>Recommended Use</h3>
      <div class="section-content">
        <?= $info['rec_use']; ?>
      </div>
    </section>

    <section class="product-specs not-recommended">
      <h3>Not Recommended For</h3>
      <div class="section-content">
        <?= $info['not_rec']; ?>
      </div>
    </section>

    <?php if($info['manufacturer_rec'] !== ""): ?>
      <section class="product-specs manufacturer-recommendations">
        <h3>Manufacturer Recommendations</h3>
        <div class="section-content">
          <?= $info['manufacturer_rec']; ?>
        </div>
      </section>
    <?php endif; ?>

  </div>

  <div class="col-sm-4">
    <section class="product-specs adheres-to">
      <h3>Adheres To</h3>
      <div class="section-content">
        <?= $info['adheres']; ?>
      </div>
    </section>

    <section class="product-specs physical-properties">
      <h3>Typical Physical Properties</h3>
      <div class="section-content">
        <?= $info['physical']; ?>

        <h4>Coverage</h4>
        <?= $info['coverage']; ?>
      </div>
    </section>

  </div>

    <div class="col-sm-4">

      <section class="product-specs application-temperature">
        <h3>Application Temperature</h3>
        <div class="section-content">
          <?= $info['app_temperature']; ?>
        </div>
      </section>

      <section class="product-specs service-temperature">
        <h3>Service Temperature</h3>
        <div class="section-content">
          <?= $info['service_temperature']; ?>
        </div>
      </section>

      <section class="product-specs specifications">
        <h3>Specifications</h3>
        <div class="section-content">
          <?= $info['specs']; ?>
        </div>
      </section>

      <section class="product-specs colors">
        <h3>Colors</h3>
        <div class="section-content">
          <ul class="colors-available-list list-unstyled list-inline">
            <?php foreach ($info['colors_available'] as $color) : ?>
              <li>
                <p>
                  <span style="background-color: <?= $color['hex']; ?>"></span>
                  <?= $color['name']; ?>
                </p>
              </li>
            <?php endforeach; ?>
          </ul>
        </div>
      </section>

    </div>

  </div>
