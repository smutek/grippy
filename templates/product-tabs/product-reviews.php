<div class="section section-reviews">
  <div class="reviews">
    <div class="pr_review_summary">
      <script type="text/javascript">
        POWERREVIEWS.display.engine(document, {
            pr_page_id: '<?= $review['page_id']; ?>',
            pr_write_review: false
          }
        );
      </script>
    </div>
  </div>
</div>
