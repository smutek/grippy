<?php if ( have_rows( 'sku_table' ) ) :

  ?>

  <div class="table-responsive">
    <table class="table table-bordered">
      <thead>
      <tr>
        <th>SKU</th>
        <th>SDS</th>
        <th>TDB</th>
      </tr>
      </thead>
      <tbody>
        <tr>
          <td>
            <?= $info['sku_list']; ?>
          </td>
          <td>
            <?php foreach ($info['data_sheets'] as $data_sheet) : ?>
            <p><a class="data-link" href="<?= $data_sheet['file']; ?>" target="_blank"><?= $data_sheet['title']; ?> <i class="fa fa-file-pdf-o" aria-hidden="true"></i></a></p>
            <?php endforeach; ?>
          </td>
          <td>
            <?php foreach ($info['data_bulletins'] as $data_bulletin) : ?>
              <p><a class="data-link" href="<?= $data_bulletin['file']; ?>" target="_blank"><?= $data_bulletin['title']; ?> <i class="fa fa-file-pdf-o" aria-hidden="true"></i></a></p>
            <?php endforeach; ?>
          </td>
        </tr>
      </tbody>
    </table>
  </div>

<?php endif; // end if have rows (sku table)

