<?php
$copy = $info['overview'];
?>

<div class="row">
  <div class="col-sm-8">
    <?= $copy; ?>
  </div>
  <?php if ($info['designations']) : ?>
    <div class="col-sm-4">
      <?php foreach ($info['designations'] as $designation) : ?>
        <div class="thumbnail-image">
          <img src="<?= $designation['url']; ?>" alt="<?= $designation['alt']; ?>">
        </div>
      <?php endforeach; ?>
    </div>
  <?php endif; ?>
</div>

