<h2>Select Size</h2>
<ul class="nav nav-tabs wtb-sku-tabs" role="tablist">
  <?php
  $count = 1;
  foreach ( $info['table_rows'] as $row ) : ?>
    <li role="presentation" class="sku-nav-tab<?php if ( $count === 1 ) {echo ' active';} ?>">
      <a href="#tab-<?= $count; ?>" aria-controls="tab-<?= $count; ?>" role="tab"
         data-toggle="tab">
        <div class="thumbnail-image">
          <img class="img-responsive" src="<?= $row['single_url']; ?>" alt="<?= $row['single_alt']; ?>">
        </div>
        <div class="sku-info">
          <p>
            <?= $row['size']; ?>
          </p>
        </div>
      </a>
    </li>
    <?php
    $count ++;
  endforeach; ?>
  <!-- Nav tabs -->
</ul>

<!-- Tab panes -->
<div class="tab-content">
  <?php
  $count = 1;
  foreach ( $info['table_rows'] as $row ) : ?>
    <div role="tabpanel" class="tab-pane<?php if ( $count === 1 ) {echo ' active';} ?>" id="tab-<?= $count; ?>">
      <ul class="sku-retailer-list">
        <?php foreach ( $row['retailers'] as $retailer ) : ?>
          <li class="sku-retailer list-unstyled list-inline">
            <a href="<?= $retailer['url']; ?>" target="_blank">
              <img class="img-responsive" src="<?= $retailer['logo_url']; ?>" alt="<?= $retailer['logo_alt']; ?>">
              <span class="sr-only">
                <?= $retailer['name']; ?>
              </span>
              <span class="buy">Buy Online</span>
            </a>
          </li>
        <?php endforeach; ?>
      </ul>

    </div>
    <?php
    $count ++;
  endforeach; ?>
</div>
