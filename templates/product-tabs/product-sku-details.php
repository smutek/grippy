<?php if ( have_rows( 'sku_table' ) ) : ?>

  <div class="table-responsive">
    <table class="table table-bordered">
      <thead>
      <tr>
        <th>Product Name</th>
        <th>SKU</th>
        <th>Size</th>
        <th>Pack</th>
        <th>Weight</th>
        <th>Dimensions</th>
        <th>Volume</th>
        <th>Per Pallet</th>
      </tr>
      </thead>
      <tbody>
      <?php foreach ( $info['table_rows'] as $row ): ?>
        <tr>
          <td><?= get_the_title(); ?></td>
          <td><?= $row['sku']; ?></td>
          <td><?= $row['size']; ?></td>
          <td><?= $row['pack']; ?></td>
          <td><?= $row['weight']; ?></td>
          <td><?= $row['dimensions']; ?></td>
          <td><?= $row['volume']; ?></td>
          <td><?= $row['pallet']; ?></td>
        </tr>
      <?php endforeach; ?>
      </tbody>
    </table>
  </div>

<?php
endif; // end if have rows (sku table)
// check if content added to sku lower
if($info['sku_lower']) : ?>

  <div class="sku-table-lower-content">
    <?= $info['sku_lower']; ?>
  </div>

<?php endif; // end if sku lower
