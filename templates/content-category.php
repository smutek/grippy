<?php
use Roots\Sage\Titles;
use Roots\Sage\Controllers;

$category = get_field( 'select_category' );
$image    = get_field( 'product_category_image', 'term_' . $category );
$color    = get_field( 'product_category_color', 'term_' . $category );
$bg       = get_field( 'cat_list_bg_image' );

$args = [
  'post_type'      => 'products',
  'posts_per_page' => - 1,
  'tax_query'      => [
    [
      'taxonomy' => 'product_categories',
      'field'    => 'id',
      'terms'    => $category
    ]
  ]
];
?>

  <header class="category-intro" style="background-image: url(<?= $bg; ?>);">

    <div class="container">
      <div class="row">
        <?php // get_template_part( 'templates/modules/breadcrumbs' ); ?>
        <div class="col-sm-6 category-image-wrap">
          <div class="thumbnail-image category-image">
            <img class="img-responsive" src="<?= $image['url']; ?>" alt="<?= $image['alt']; ?>">
          </div>
        </div>
        <div class="col-sm-6 category-content-wrap">
          <header class="category-header" style="background: <?= $color; ?>">
            <h1><?= Titles\title(); ?></h1>
          </header>
          <div class="category-content">
            <?php the_content(); ?>
          </div>
        </div>
      </div>
    </div>
  </header>

<?php
$query = new WP_Query( $args );
if ( $query->have_posts() ) :
  ?>

  <section class="product-list">
    <div class="container">
      <header class="products-list-header">
        <h2>Products</h2>
      </header>

      <?php
      while ( $query->have_posts() ) : $query->the_post();
        $branding = Controllers\productBranding();
        $info     = Roots\Sage\ProductTabsController\productInfo();
        $review   = Controllers\powerReviews();
        ?>
        <div class="product">
          <div class="row">

            <div class="col-sm-4 col-md-2 product-image">
              <div class="thumbnail-image">
                <img class="img-responsive" src="<?= $info['img_url']; ?>" alt="<?= $info['img_alt']; ?>">
              </div>
            </div>

            <div class="col-sm-8 col-md-7 col-lg-4 product-info">
              <h3><a href="<?= get_the_permalink(); ?>"><?php the_title(); ?></a></h3>
              <?php if ( get_field( 'reviews_show_tab' ) ) : ?>
                <div class="reviews">
                  <?php include( locate_template( 'templates/modules/review-snippet.php' ) ); ?>
                </div>
              <?php endif; ?>

              <?= $info['excerpt']; ?>
            </div>

            <div class="col-sm-4 col-md-3 col-lg-2 product-sizes">
              <p><strong>Sizes Available</strong></p>
              <ul>
                <?php foreach ( $info['table_rows'] as $table_row ) : ?>
                  <li><?= $table_row['size']; ?></li>
                <?php endforeach; ?>
              </ul>
            </div>

            <div class="col-sm-8 col-sm-offset-4 col-md-offset-2 col-lg-offset-0 col-lg-4 product-links">
              <p><a class="btn btn-more" href="<?php the_permalink(); ?>">Learn More</a></p>
              <p><a class="btn btn-wtb" href="<?php the_permalink(); ?>#where-to-buy">Where To Buy <i class="fa fa-map-marker" aria-hidden="true"></i></a></p>
            </div>

            <footer class="col-sm-12 benefits-wrap">
              <ul class="list-unstyled product-benefits">
                <?php foreach ( $branding['benefits'] as $benefit ) : ?>
                  <li>
                    <img class="img-responsive" src="<?= $benefit['iconURL']; ?>" alt="<?= $benefit['iconALT']; ?>">
                    <p><?= $benefit['name']; ?></p>
                  </li>
                <?php endforeach; ?>
              </ul>
            </footer>

          </div>
        </div>
      <?php endwhile;
      wp_reset_postdata(); ?>
    </div>
  </section>

<?php endif; // end if posts
