<?php
use Roots\Sage\Controllers;

$brand          = Controllers\footerLogo();
$brandSecondary = Controllers\secondaryLogo();
$copyRight      = Controllers\copyright();
?>
<footer class="content-info">
  <div class="upper-footer">
    <div class="container">


          <div class="footer-brand footer-brand-primary">
            <?= $brand; ?>
          </div>


          <?php if ( has_nav_menu( 'product_navigation' ) ) : ?>
            <nav class="nav product-nav" role="navigation">
              <?php
              wp_nav_menu( [ 'theme_location' => 'product_navigation', 'menu_class' => 'nav nav-pills' ] );
              ?>
            </nav>
          <?php endif; ?>

          <div class="footer-brand footer-brand-secondary">
            <?= $brandSecondary; ?>
          </div>

      </div>
    </div>

  <div class="lower-footer">
    <div class="container">

      <?php if ( has_nav_menu( 'footer_navigation' ) ) : ?>
        <nav class="nav footer-nav" role="navigation">
          <?php
          wp_nav_menu( [ 'theme_location' => 'footer_navigation', 'menu_class' => 'nav' ] );
          ?>
        </nav>
      <?php endif; ?>

      <?php if ( $copyRight ) : ?>
        <div class="copyright">
          <p><small><?= $copyRight; ?></small></p>
        </div>
      <?php endif; ?>

    </div>
  </div>
</footer>
