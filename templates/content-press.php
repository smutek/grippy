<?php
$link  = get_field( 'press_link' );
$image = get_field( 'press_image' );
$copy  = get_field( 'press_excerpt' );
?>
<article <?php post_class( 'col-sm-12 col-lg-6' ); ?>>
  <div class="thumbnail-image">
    <img class="img-responsive" src="<?= $image['url']; ?>" alt="<?= $image['alt']; ?>">
  </div>
  <div class="entry-content">
    <header>
      <h2 class="entry-title"><?php the_title(); ?></h2>
    </header>
    <div class="entry-summary">
      <?= $copy; ?>
    </div>
    <footer>
      <a class="btn btn-underline" target="_blank" href="<?= $link; ?>">Read More</a>
    </footer>
  </div>
</article>
