<?php $brand = get_custom_logo(); ?>
<header class="banner navbar navbar-inverse navbar-static-top" role="banner">

  <div class="container">

    <div class="navbar-header row">
      <div class="col-xs-3 burger">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
          <span class="navbar-toggle-text"><?= __( 'Menu', 'sage' ); ?></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
      </div>
      <div class="col-xs-9 col-sm-12">
        <div class="navbar-brand"><?= $brand; ?></div>
      </div>
    </div>

    <?php if ( has_nav_menu( 'primary_navigation' ) ) : ?>
      <nav class="collapse navbar-collapse navbar-right main-nav" role="navigation">
        <?php
        wp_nav_menu( array(
            'theme_location'  => 'primary_navigation',
            'depth'           => 3,
            'container'       => 'div',
            'container_class' => 'collapse navbar-collapse',
            'menu_class'      => 'nav navbar-nav yamm',
            'fallback_cb'     => 'Yamm_Nav_Walker_menu_fallback',
            'walker'          => new Yamm_Nav_Walker()
          )
        );
        ?>
      </nav>
    <?php endif; ?>
    <?php if ( has_nav_menu( 'social_navigation' ) ) : ?>
      <div class="social-wrap">
        <nav class="navbar-right collapse navbar-collapse social-nav" role="navigation">
          <?php
          wp_nav_menu( [
            'theme_location' => 'social_navigation',
            'menu_class'     => 'nav navbar-nav nav-social',
            'link_before'    => '<span class="sr-only">',
            'link_after'     => '</span>'
          ] );
          ?>
        </nav>
      </div>
    <?php endif; ?>
  </div>
</header>
