<div class="page-content">
  <?php the_content(); ?>

  <?php if (is_page('technical-data'))
    get_template_part('templates/sections/page', 'technical-data'); ?>
</div>
