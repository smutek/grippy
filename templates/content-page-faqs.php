<?php
use Roots\Sage\Controllers;

$sections = Controllers\faq_sections();
?>
<section class="faqs container">
  <div class="row">

    <!-- Nav tabs -->
    <div class="col-sm-4 col-md-3">
      <header class="faq-nav-header">
        <h3><?= get_field( 'faq_navigation_title' );; ?></h3>
      </header>
      <ul class="nav nav-stacked faq-nav" role="tablist">
        <?php foreach ( $sections as $section ) : ?>
          <li role="presentation" id="faq-<?= $section['slug']; ?>" class="<?= $section['classes']; ?>">
            <a href="#<?= $section['slug']; ?>" aria-controls="<?= $section['slug']; ?>" role="tab"
               data-toggle="tab"><?= $section['title']; ?></a>
          </li>
        <?php endforeach; ?>
      </ul>

    </div>

    <!-- Tab panes -->
    <div class="col-sm-8 col-md-9">
      <div class="faq-tab-content tab-content">
        <?php foreach ( $sections as $section ) : ?>
          <div role="tabpanel" class="<?= $section['classes']; ?> tab-pane" id="<?= $section['slug']; ?>">
            <?php include( locate_template( 'templates/sections/page-faqs.php' ) ); ?>
          </div>
        <?php endforeach; ?>
      </div>
    </div>
  </div>
</section>

