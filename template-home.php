<?php
/**
 * Template Name: Home
 */

while (have_posts()) : the_post();
  get_template_part('templates/sections/home', 'products');
  get_template_part('templates/sections/home', 'media-grid');
endwhile;
