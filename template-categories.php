<?php
/**
 * Template Name: Product Categories
 */
while ( have_posts() ) : the_post();
  get_template_part( 'templates/content', 'category' );
endwhile;
