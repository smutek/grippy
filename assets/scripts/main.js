/* ========================================================================
 * DOM-based Routing
 * Based on http://goo.gl/EUTi53 by Paul Irish
 *
 * Only fires on body classes that match. If a body class contains a dash,
 * replace the dash with an underscore when adding it to the object below.
 *
 * .noConflict()
 * The routing is enclosed within an anonymous function so that you can
 * always reference jQuery with $, even when in .noConflict() mode.
 * ======================================================================== */

(function($) {

  /***** Equal Height Function *****/
  var equalheight = function (container) {

    var currentTallest = 0,
      currentRowStart = 0,
      rowDivs = [],
      $el,
      $currentDiv,
      topPosition = 0;
    $(container).each(function () {

      $el = $(this);
      $($el).height('auto');
      topPostion = $el.position().top;

      if (currentRowStart !== topPostion) {
        for (currentDiv = 0; currentDiv < rowDivs.length; currentDiv++) {
          rowDivs[currentDiv].height(currentTallest);
        }
        rowDivs.length = 0; // empty the array
        currentRowStart = topPostion;
        currentTallest = $el.height();
        rowDivs.push($el);
      } else {
        rowDivs.push($el);
        currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);
      }
      for (currentDiv = 0; currentDiv < rowDivs.length; currentDiv++) {
        rowDivs[currentDiv].height(currentTallest);
      }
    });
  };

  // Use this variable to set up the common and page specific functions. If you
  // rename this variable, you will also need to rename the namespace below.
  var Sage = {
    // All pages
    'common': {
      init: function() {
        // JavaScript to be fired on all pages

        // fitvids
        $([".video-wrap"]).fitVids();

        // stop video playback on modal close
        // http://stackoverflow.com/a/25069916
        $(".video-modal").on('hidden.bs.modal', function (e) {

          var id = $(this).attr('id');
          var target = "#" + id + ".video-modal iframe";

          $(target).attr("src", $(target).attr("src"));
        });

        // Items to run the equal height function on
        var $equalHeightArray = [
          '.home-product',
          '.media-grid-row .media-grid-item'
        ];
        // Iterate through the array, if the element is found run the function on
        // window load and on window resize.
        $.each($equalHeightArray, function(index, value){
          if(value) {

            $(window).load(function () {
              var $window = $(window).width();

              equalheight(value);

              $(window).resize(function () {
                equalheight(value);
              });
            });
          }
        });

      },
      finalize: function() {
        // JavaScript to be fired on all pages, after page specific JS is fired
      }
    },
    // Home page
    'home': {
      init: function() {
        // JavaScript to be fired on the home page

      },
      finalize: function() {
        // JavaScript to be fired on the home page, after the init JS
      }
    },
    // Single products page
    'single_products': {
      init: function() {
        // JavaScript to be fired on the products single page

        // where to buy / hash jump
        $(document).ready(function(){
          if(window.location.hash === "#where-to-buy") {
            // http://getbootstrap.com/javascript/#tabs-usage
            $('.product-nav-tabs a[href="#where-to-buy"]').tab('show');

            $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
              console.log('shown');
            });

            $(function(){
              $('html, body').animate({
                scrollTop: $('.product-nav-tabs').offset().top
              }, 500);
              return false;
            });

          }
        });

        // slick
        var
          productSlider = '.product-slider',
          productSliderThumbs = '.product-slider-thumbnails',
          thumbs = $('.product-slider-thumbnails li').length;
        $(productSlider).slick({
          slidesToShow: 1,
          slidesToScroll: 1,
          arrows: false,
          fade: false,
          asNavFor: productSliderThumbs
        });


        $(productSliderThumbs).slick({
          slidesToShow: thumbs,
          slidesToScroll: 1,
          asNavFor: productSlider,
          dots: false,
          focusOnSelect: true
        });

        // Remove active class from all thumbnail slides
        $(productSliderThumbs + ' .slick-slide').removeClass('slick-active');

        // Set active class to first thumbnail slides
        $(productSliderThumbs + ' .slick-slide').eq(0).addClass('slick-active');

        // On before slide change match active thumbnail to current slide
        $(productSlider).on('beforeChange', function (event, slick, currentSlide, nextSlide) {
          var mySlideNumber = nextSlide;
          $(productSliderThumbs + ' .slick-slide').removeClass('slick-active');
          $(productSliderThumbs + ' .slick-slide').eq(mySlideNumber).addClass('slick-active');
        });


      }
    },
    // Contact page
    'contact': {
      init: function() {
        // JavaScript to be fired on the contact page
      }
    },
    'where_to_buy': {
      init: function () {
        // open close product categories on click
        // http://stackoverflow.com/a/32456872
        $("[data-collapse-group]").on('show.bs.collapse', function () {
          var $this = $(this);
          var thisCollapseAttr = $this.attr('data-collapse-group');
          $("[data-collapse-group='" + thisCollapseAttr + "']").not($this).collapse('hide');
        });

      }
    }
  };

  // The routing fires all common scripts, followed by the page specific scripts.
  // Add additional events for more control over timing e.g. a finalize event
  var UTIL = {
    fire: function(func, funcname, args) {
      var fire;
      var namespace = Sage;
      funcname = (funcname === undefined) ? 'init' : funcname;
      fire = func !== '';
      fire = fire && namespace[func];
      fire = fire && typeof namespace[func][funcname] === 'function';

      if (fire) {
        namespace[func][funcname](args);
      }
    },
    loadEvents: function() {
      // Fire common init JS
      UTIL.fire('common');

      // Fire page-specific init JS, and then finalize JS
      $.each(document.body.className.replace(/-/g, '_').split(/\s+/), function(i, classnm) {
        UTIL.fire(classnm);
        UTIL.fire(classnm, 'finalize');
      });

      // Fire common finalize JS
      UTIL.fire('common', 'finalize');
    }
  };

  // Load Events
  $(document).ready(UTIL.loadEvents);

})(jQuery); // Fully reference jQuery after this point.
