<?php
/**
 * Template Name: Where To Buy
 */
?>

<?php while (have_posts()) : the_post(); ?>
  <?php get_template_part('templates/page', 'header'); ?>
  <?php get_template_part('templates/content', 'where-to-buy'); ?>
<?php endwhile; ?>
