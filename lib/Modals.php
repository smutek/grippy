<?php
namespace Roots\Sage\Modals;

use Roots\Sage\Wrapper\SageWrapping;

/**
 * Class Modal
 * @package Roots\Sage\Modal
 *
 * Usage  - $modal = new Modal($video, $modalID);
 *
 * This will instantiate a video modal object and output a modal with the correct video
 * immediately after the footer.
 *
 * The $video parameter is the output of an acf get_field or get_subfield call to an oEmbed
 * field.
 *
 * The $modalID is a unique ID for this particular modal. It's primarily important when
 * displaying multiple video modals on one page as it ties into the data-target and such
 *
 * The main benefit to using this class is that it outputs the modal to the footer automagically
 *
 */
class Modal {

  /**
   * The video object being passed along
   * @var
   */
  public $video;

  /**
   * @var
   */
  public $modalID;

  /**
   * Modal constructor.
   *
   * @param $video
   * @param $modalID
   */
  public function __construct( $video, $modalID ) {

    // store the video as a property of the object instance
    $this->video = $video;
    // store the modal ID as property of the object instance
    $this->modalID = $modalID;

    // render the modal to the footer area. Uses the outputModal method.
    // note, after_footer was added manually to base.php, and will need to be added to
    // additional base.php files. When creating a new template just add do_action('after_footer')
    // below the wp_footer() function in your base.php file
    add_action( 'after_footer', [ $this, 'outputModal' ] );
  }
  // **** end __constructor

  /**
   * The file path to the modal template
   *
   * @return string
   */
  public function filePath() {
    $path     = 'templates/modules/modal-video.php';
    $template = new SageWrapping( $path );

    return $template;
  }

  /**
   * Returns the rendered meodal template markup
   * @return string
   */
  public function renderModal() {
    ob_start();
    include $this->filePath();
    $output = ob_get_clean();

    return $output;
  }

  /**
   * Echos the rendered modal markup to the template
   *
   */
  public function outputModal() {
    $modal = $this->renderModal();
    echo $modal;
  }

}
