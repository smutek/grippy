<?php

// Register Custom Post Type
function retailers() {

$labels = array(
'name'                  => _x( 'Retailers', 'Post Type General Name', 'sage' ),
'singular_name'         => _x( 'Retailer', 'Post Type Singular Name', 'sage' ),
'menu_name'             => __( 'Retailers', 'sage' ),
'name_admin_bar'        => __( 'Retailer', 'sage' ),
'archives'              => __( 'Item Archives', 'sage' ),
'attributes'            => __( 'Item Attributes', 'sage' ),
'parent_item_colon'     => __( 'Parent Item:', 'sage' ),
'all_items'             => __( 'All Retailers', 'sage' ),
'add_new_item'          => __( 'Add New Retailer', 'sage' ),
'add_new'               => __( 'Add New Retailer', 'sage' ),
'new_item'              => __( 'New Retailer', 'sage' ),
'edit_item'             => __( 'Edit Retailer', 'sage' ),
'update_item'           => __( 'Update Retailer', 'sage' ),
'view_item'             => __( 'View Retailer', 'sage' ),
'view_items'            => __( 'View Retailers', 'sage' ),
'search_items'          => __( 'Search Retailer', 'sage' ),
'not_found'             => __( 'Not found', 'sage' ),
'not_found_in_trash'    => __( 'Not found in Trash', 'sage' ),
'featured_image'        => __( 'Retailer Logo', 'sage' ),
'set_featured_image'    => __( 'Set retailer logo', 'sage' ),
'remove_featured_image' => __( 'Remove retailer logo', 'sage' ),
'use_featured_image'    => __( 'Use as retailer logo', 'sage' ),
'insert_into_item'      => __( 'Insert into item', 'sage' ),
'uploaded_to_this_item' => __( 'Uploaded to this item', 'sage' ),
'items_list'            => __( 'Retailers list', 'sage' ),
'items_list_navigation' => __( 'Retailers list navigation', 'sage' ),
'filter_items_list'     => __( 'Filter retailers list', 'sage' ),
);
$args = array(
'label'                 => __( 'Retailer', 'sage' ),
'description'           => __( 'List of retailers', 'sage' ),
'labels'                => $labels,
'supports'              => array( 'title', 'author', 'revisions', ),
'hierarchical'          => false,
'public'                => true,
'show_ui'               => true,
'show_in_menu'          => true,
'menu_position'         => 5,
'menu_icon'             => 'dashicons-building',
'show_in_admin_bar'     => true,
'show_in_nav_menus'     => true,
'can_export'            => true,
'has_archive'           => false,
'exclude_from_search'   => true,
'publicly_queryable'    => false,
'capability_type'       => 'page',
'show_in_rest'          => true,
);
register_post_type( 'retailers', $args );

}
add_action( 'init', 'retailers', 0 );
