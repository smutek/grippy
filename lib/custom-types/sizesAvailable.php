<?php

// Register Custom Taxonomy
function sizes() {

  $labels = array(
    'name'                       => _x( 'Sizes', 'Taxonomy General Name', 'sage' ),
    'singular_name'              => _x( 'Size', 'Taxonomy Singular Name', 'sage' ),
    'menu_name'                  => __( 'Sizes Available', 'sage' ),
    'all_items'                  => __( 'All Sizes', 'sage' ),
    'parent_item'                => __( 'Parent Size', 'sage' ),
    'parent_item_colon'          => __( 'Parent Item:', 'sage' ),
    'new_item_name'              => __( 'New Size Name', 'sage' ),
    'add_new_item'               => __( 'Add New Size', 'sage' ),
    'edit_item'                  => __( 'Edit Size', 'sage' ),
    'update_item'                => __( 'Update Size', 'sage' ),
    'view_item'                  => __( 'View Size', 'sage' ),
    'separate_items_with_commas' => __( 'Separate sizes with commas', 'sage' ),
    'add_or_remove_items'        => __( 'Add or remove sizes', 'sage' ),
    'choose_from_most_used'      => __( 'Choose from the most used', 'sage' ),
    'popular_items'              => __( 'Popular Sizes', 'sage' ),
    'search_items'               => __( 'Search Sizes', 'sage' ),
    'not_found'                  => __( 'Not Found', 'sage' ),
    'no_terms'                   => __( 'No sizes', 'sage' ),
    'items_list'                 => __( 'Sizes list', 'sage' ),
    'items_list_navigation'      => __( 'Sizes list navigation', 'sage' ),
  );
  $rewrite = array(
    'slug'                       => 'sizes',
    'with_front'                 => true,
    'hierarchical'               => false,
  );
  $args = array(
    'labels'                     => $labels,
    'hierarchical'               => false,
    'public'                     => true,
    'show_ui'                    => true,
    'show_admin_column'          => true,
    'show_in_nav_menus'          => true,
    'show_tagcloud'              => false,
    'rewrite'                    => $rewrite,
    'show_in_rest'               => true,
    'meta_box_cb'                => false,
  );
  register_taxonomy( 'sizes', array( 'products' ), $args );

}
add_action( 'init', 'sizes', 0 );
