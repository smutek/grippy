<?php

// Register Custom Post Type
function press() {

  $labels = array(
    'name'                  => _x( 'Press Links', 'Post Type General Name', 'sage' ),
    'singular_name'         => _x( 'Press Link', 'Post Type Singular Name', 'sage' ),
    'menu_name'             => __( 'Press Links', 'sage' ),
    'name_admin_bar'        => __( 'Press Link', 'sage' ),
    'archives'              => __( 'Press Archives', 'sage' ),
    'attributes'            => __( 'Press Attributes', 'sage' ),
    'parent_item_colon'     => __( 'Parent Item:', 'sage' ),
    'all_items'             => __( 'All Press Links', 'sage' ),
    'add_new_item'          => __( 'Add New Press Link', 'sage' ),
    'add_new'               => __( 'Add Press Link', 'sage' ),
    'new_item'              => __( 'New Press Link', 'sage' ),
    'edit_item'             => __( 'Edit Press Link', 'sage' ),
    'update_item'           => __( 'Update Press Link', 'sage' ),
    'view_item'             => __( 'View Press Link', 'sage' ),
    'view_items'            => __( 'View Press Links', 'sage' ),
    'search_items'          => __( 'Search Press Link', 'sage' ),
    'not_found'             => __( 'Not found', 'sage' ),
    'not_found_in_trash'    => __( 'Not found in Trash', 'sage' ),
    'featured_image'        => __( 'Featured Image', 'sage' ),
    'set_featured_image'    => __( 'Set featured image', 'sage' ),
    'remove_featured_image' => __( 'Remove featured image', 'sage' ),
    'use_featured_image'    => __( 'Use as featured image', 'sage' ),
    'insert_into_item'      => __( 'Insert into item', 'sage' ),
    'uploaded_to_this_item' => __( 'Uploaded to this item', 'sage' ),
    'items_list'            => __( 'Press links list', 'sage' ),
    'items_list_navigation' => __( 'Press links navigation', 'sage' ),
    'filter_items_list'     => __( 'Filter press list', 'sage' ),
  );
  $args = array(
    'label'                 => __( 'Press Link', 'sage' ),
    'description'           => __( 'Post type for press links', 'sage' ),
    'labels'                => $labels,
    'supports'              => array( 'title', 'author', 'revisions', ),
    'hierarchical'          => false,
    'public'                => false,
    'show_ui'               => true,
    'show_in_menu'          => true,
    'menu_position'         => 5,
    'menu_icon'             => 'dashicons-thumbs-up',
    'show_in_admin_bar'     => true,
    'show_in_nav_menus'     => false,
    'can_export'            => true,
    'has_archive'           => true,
    'exclude_from_search'   => true,
    'publicly_queryable'    => true,
    'capability_type'       => 'page',
    'show_in_rest'          => true,
  );
  register_post_type( 'press', $args );

}
add_action( 'init', 'press', 0 );
