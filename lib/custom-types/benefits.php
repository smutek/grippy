<?php

// Register Custom Taxonomy
function benefitsFeatures() {

  $labels = array(
    'name'                       => _x( 'Benefits & Features', 'Taxonomy General Name', 'sage' ),
    'singular_name'              => _x( 'Benefit', 'Taxonomy Singular Name', 'sage' ),
    'menu_name'                  => __( 'Benefits & Features', 'sage' ),
    'all_items'                  => __( 'All Benefits', 'sage' ),
    'parent_item'                => __( 'Parent Benefits', 'sage' ),
    'parent_item_colon'          => __( 'Parent Item:', 'sage' ),
    'new_item_name'              => __( 'New Benefit', 'sage' ),
    'add_new_item'               => __( 'Add New Benefit', 'sage' ),
    'edit_item'                  => __( 'Edit Benefit', 'sage' ),
    'update_item'                => __( 'Update Benefit', 'sage' ),
    'view_item'                  => __( 'View Benefit', 'sage' ),
    'separate_items_with_commas' => __( 'Separate benefits & features with commas', 'sage' ),
    'add_or_remove_items'        => __( 'Add or remove benefits', 'sage' ),
    'choose_from_most_used'      => __( 'Choose from the most used', 'sage' ),
    'popular_items'              => __( 'Popular Benefits & Features', 'sage' ),
    'search_items'               => __( 'Search Benefits & Features', 'sage' ),
    'not_found'                  => __( 'Not Found', 'sage' ),
    'no_terms'                   => __( 'No items', 'sage' ),
    'items_list'                 => __( 'Benefits & Features list', 'sage' ),
    'items_list_navigation'      => __( 'Benefits & Features list navigation', 'sage' ),
  );
  $rewrite = array(
    'slug'                       => 'benefits',
    'with_front'                 => true,
    'hierarchical'               => false,
  );
  $args = array(
    'labels'                     => $labels,
    'hierarchical'               => false,
    'public'                     => true,
    'show_ui'                    => true,
    'show_admin_column'          => true,
    'show_in_nav_menus'          => true,
    'show_tagcloud'              => false,
    'rewrite'                    => $rewrite,
    'show_in_rest'               => true,
    'meta_box_cb'                => false,
  );
  register_taxonomy( 'benefits_features', array( 'products' ), $args );

}
add_action( 'init', 'benefitsFeatures', 0 );
