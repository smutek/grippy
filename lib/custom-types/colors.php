<?php

// Register Custom Taxonomy
function colors() {

  $labels = array(
    'name'                       => _x( 'Colors', 'Taxonomy General Name', 'sage' ),
    'singular_name'              => _x( 'Color', 'Taxonomy Singular Name', 'sage' ),
    'menu_name'                  => __( 'Colors Available', 'sage' ),
    'all_items'                  => __( 'All Colors', 'sage' ),
    'parent_item'                => __( 'Parent Size', 'sage' ),
    'parent_item_colon'          => __( 'Parent Item:', 'sage' ),
    'new_item_name'              => __( 'New Size Name', 'sage' ),
    'add_new_item'               => __( 'Add New Color', 'sage' ),
    'edit_item'                  => __( 'Edit Color', 'sage' ),
    'update_item'                => __( 'Update Color', 'sage' ),
    'view_item'                  => __( 'View Color', 'sage' ),
    'separate_items_with_commas' => __( 'Separate colors with commas', 'sage' ),
    'add_or_remove_items'        => __( 'Add or remove colors', 'sage' ),
    'choose_from_most_used'      => __( 'Choose from the most used colors', 'sage' ),
    'popular_items'              => __( 'Popular Colors', 'sage' ),
    'search_items'               => __( 'Search Colors', 'sage' ),
    'not_found'                  => __( 'Not Found', 'sage' ),
    'no_terms'                   => __( 'No colors', 'sage' ),
    'items_list'                 => __( 'Colors list', 'sage' ),
    'items_list_navigation'      => __( 'Colors list navigation', 'sage' ),
  );
  $rewrite = array(
    'slug'                       => 'colors',
    'with_front'                 => true,
    'hierarchical'               => false,
  );
  $args = array(
    'labels'                     => $labels,
    'hierarchical'               => false,
    'public'                     => false,
    'show_ui'                    => true,
    'show_admin_column'          => true,
    'show_in_nav_menus'          => true,
    'show_tagcloud'              => false,
    'rewrite'                    => $rewrite,
    'show_in_rest'               => true,
    'meta_box_cb'                => false,
  );
  register_taxonomy( 'colors_available', array( 'products' ), $args );

}
add_action( 'init', 'colors', 0 );
