<?php

// Register Custom Post Type
function products() {

  $labels = array(
    'name'                  => _x( 'Products', 'Post Type General Name', 'sage' ),
    'singular_name'         => _x( 'Product', 'Post Type Singular Name', 'sage' ),
    'menu_name'             => __( 'Products', 'sage' ),
    'name_admin_bar'        => __( 'Products', 'sage' ),
    'archives'              => __( 'Product Archives', 'sage' ),
    'attributes'            => __( 'Product Attributes', 'sage' ),
    'parent_item_colon'     => __( 'Parent Product:', 'sage' ),
    'all_items'             => __( 'All products', 'sage' ),
    'add_new_item'          => __( 'Add New Product', 'sage' ),
    'add_new'               => __( 'Add Product', 'sage' ),
    'new_item'              => __( 'New Product', 'sage' ),
    'edit_item'             => __( 'Edit product', 'sage' ),
    'update_item'           => __( 'Update product', 'sage' ),
    'view_item'             => __( 'View Product', 'sage' ),
    'view_items'            => __( 'View products', 'sage' ),
    'search_items'          => __( 'Search Product', 'sage' ),
    'not_found'             => __( 'Not found', 'sage' ),
    'not_found_in_trash'    => __( 'Not found in Trash', 'sage' ),
    'featured_image'        => __( 'Main Product Image', 'sage' ),
    'set_featured_image'    => __( 'Set main image', 'sage' ),
    'remove_featured_image' => __( 'Remove main image', 'sage' ),
    'use_featured_image'    => __( 'Use as main image', 'sage' ),
    'insert_into_item'      => __( 'Insert into product', 'sage' ),
    'uploaded_to_this_item' => __( 'Uploaded to this product', 'sage' ),
    'items_list'            => __( 'Products list', 'sage' ),
    'items_list_navigation' => __( 'Products list navigation', 'sage' ),
    'filter_items_list'     => __( 'Filter products list', 'sage' ),
  );
  $args = array(
    'label'                 => __( 'Product', 'sage' ),
    'description'           => __( 'Products Post Type', 'sage' ),
    'labels'                => $labels,
    'supports'              => array( 'title', 'author', 'thumbnail', 'revisions',),
    'taxonomies'            => array( 'product_category', 'sizes_available', 'benefits_features' ),
    'hierarchical'          => false,
    'public'                => true,
    'show_ui'               => true,
    'show_in_menu'          => true,
    'menu_position'         => 5,
    'menu_icon'             => 'dashicons-cart',
    'show_in_admin_bar'     => true,
    'show_in_nav_menus'     => true,
    'can_export'            => true,
    'has_archive'           => false,
    'exclude_from_search'   => false,
    'publicly_queryable'    => true,
    'capability_type'       => 'page',
    'show_in_rest'          => true,
  );
  register_post_type( 'products', $args );

}
add_action( 'init', 'products', 0 );
