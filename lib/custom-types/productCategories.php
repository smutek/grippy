<?php

// Register Custom Taxonomy
function productCategories() {

  $labels = array(
    'name'                       => _x( 'Product Categories', 'Taxonomy General Name', 'sage' ),
    'singular_n
    ame'              => _x( 'Product Category', 'Taxonomy Singular Name', 'sage' ),
    'menu_name'                  => __( 'Product Category', 'sage' ),
    'all_items'                  => __( 'All Categories', 'sage' ),
    'parent_item'                => __( 'Parent Category', 'sage' ),
    'parent_item_colon'          => __( 'Parent Item:', 'sage' ),
    'new_item_name'              => __( 'New Category Name', 'sage' ),
    'add_new_item'               => __( 'Add New Category', 'sage' ),
    'edit_item'                  => __( 'Edit Category', 'sage' ),
    'update_item'                => __( 'Update Category', 'sage' ),
    'view_item'                  => __( 'View Category', 'sage' ),
    'separate_items_with_commas' => __( 'Separate categories with commas', 'sage' ),
    'add_or_remove_items'        => __( 'Add or remove categories', 'sage' ),
    'choose_from_most_used'      => __( 'Choose from the most used', 'sage' ),
    'popular_items'              => __( 'Popular Categories', 'sage' ),
    'search_items'               => __( 'Search Categories', 'sage' ),
    'not_found'                  => __( 'Not Found', 'sage' ),
    'no_terms'                   => __( 'No items', 'sage' ),
    'items_list'                 => __( 'Categories list', 'sage' ),
    'items_list_navigation'      => __( 'Categories list navigation', 'sage' ),
  );
  $rewrite = array(
    'slug'                       => 'product-categories',
    'with_front'                 => false,
    'hierarchical'               => false,
  );
  $args = array(
    'labels'                     => $labels,
    'hierarchical'               => false,
    'public'                     => false,
    'show_ui'                    => true,
    'show_admin_column'          => true,
    'show_in_nav_menus'          => true,
    'show_tagcloud'              => false,
    'rewrite'                    => $rewrite,
    'show_in_rest'               => true,
    'meta_box_cb'                => false,
  );
  register_taxonomy( 'product_categories', array( 'products' ), $args );

}
add_action( 'init', 'productCategories', 0 );
