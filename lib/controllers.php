<?php

namespace Roots\Sage\Controllers;

use Roots\Sage\Modals\Modal;
use Roots\Sage\Extras;
use Roots\Sage\ProductTabsController;

/**
 * Site Brand
 *
 * Use native WordPress site logo with custom (bootstrap friendly) markup
 * Falls back to text title if logo is not set.
 *
 * @param $html
 *
 * @return string
 */
function site_brand( $html ) {
  // grab the site name as set in customizer options
  $site = get_bloginfo( 'name' );
  // Wrap the site name in an H1 if on home, in a paragraph tag if not.
  is_front_page() ? $title = '<h1>' . $site . '</h1>' : $title = '<p>' . $site . '</p>';
  // Grab the home URL
  $home = esc_url( home_url( '/' ) );
  // Class for the link
  $class = 'navbar-brand';
  // Set anchor content to $title
  $content = $title;
  // Check if there is a custom logo set in customizer options...
  if ( has_custom_logo() ) {
    // get the URL to the logo
    $logo    = wp_get_attachment_image( get_theme_mod( 'custom_logo' ), 'full', false, array(
      'class'    => 'brand-logo',
      'itemprop' => 'logo',
    ) );
    $content = $logo;
    $content .= '<span class="sr-only">' . $title . '</span>';
  }
  // setup the markup
  $html = sprintf( '<a href="%1$s" class="%2$s" rel="home" itemprop="url">%3$s</a>', $home, $class, $content );

  // return the result to WordPress
  return $html;
}

add_filter( 'get_custom_logo', __NAMESPACE__ . '\\site_brand' );

/**
 * Secondary logo.
 *
 * Works similar to the site brand function, exception being it
 * uses get_theme_mod (custom customizer)
 *
 */

function secondaryLogo() {
  $logoURL = get_theme_mod( 'secondary_logo' );
  $site    = get_bloginfo( 'name' );
  $link    = get_theme_mod( 'secondary_logo_link' );

  $output = '<p>' . $site . '</p>';

  if ( ! empty( $logoURL ) ) {
    $output = '<p class="sr-only">' . $site . '</p>';
    $output .= '<img src="' . $logoURL . '" alt="DAP Secondary Logo">';
  }

  if ( ! empty( $link ) ) {

    $html = sprintf( '<a href="%1$s" target="_blank" itemprop="url">%2$s</a>', $link, $output );

    $output = $html;

  }

  return $output;
}


/**
 * Footer logo.
 */

function footerLogo() {
  $logo   = wp_get_attachment_image( get_theme_mod( 'custom_logo' ), 'full', false, array(
    'class'    => 'brand-logo',
    'itemprop' => 'logo',
  ) );
  $site   = get_bloginfo( 'name' );
  $output = '<p>' . $site . '</p>';
  if ( ! empty( $logo ) ) {
    $output = $logo;
  }

  return $output;
}

/**
 * Returns an array with business info.
 *
 * @return array
 */
function businessInfo() {
  $address1 = get_theme_mod( 'address_line_1' );
  $address2 = get_theme_mod( 'address_line_2' );
  $address3 = get_theme_mod( 'address_line_3' );
  $phone    = get_theme_mod( 'phone_setting' );
  $fax      = get_theme_mod( 'fax_setting' );
  $email    = get_theme_mod( 'email_setting' );
  get_theme_mod( 'secondary_logo' ) ? $logo = get_theme_mod( 'secondary_logo' ) : $logo = get_custom_logo();
  $info = [
    'address1' => $address1,
    'address2' => $address2,
    'address3' => $address3,
    'phone'    => $phone,
    'fax'      => $fax,
    'email'    => $email,
    'logo'     => $logo
  ];

  return $info;
}

/**
 * Site copyright notice
 *
 * @return string
 */
function copyright() {
  $businessName = get_theme_mod( 'business_name' );
  ! empty( $businessName ) ? $name = $businessName : $name = get_bloginfo( 'name' );
  $output = '&copy; ' . date( 'Y' ) . ' ' . $name;

  return $output;
}

/**
 * Media Grid
 *
 * Returns an array with media grid items
 *
 * @param $field
 * @param $id
 *
 * @return array
 */
function mediaGrid( $field, $id ) {

  // blank array to hold media items
  $items = [];
  // init counter, used for classes and also for generating unique identifiers for modals
  $count = 1;

  // run through the repeater. Not checking if conditional because the initial function call
  // should already be nested in an if.
  while ( have_rows( $field, $id ) ) : the_row();

    // empty array to hold item params
    $item = [];
    // grab and store item info
    $item['title'] = get_sub_field( 'home_media_title', $id );
    $item['copy']  = get_sub_field( 'home_media_copy', $id );
    $item['bg']    = get_sub_field( 'home_media_bg', $id );
    $item['type']  = get_sub_field( 'home_media_content_type', $id );

    // button text gets passed along
    $buttonText = get_sub_field( 'home_media_button_text', $id );

    // This determines how the grid outputs on front end.
    // It's not dynamic, grid items are limited to 4 and
    // the layout is constrained to the design.
    //
    // if count is one or 4, col-4, else col 8

    $base_class = 'media-grid-item ';

    $count === 1 || $count === 4 ? $col = 'col-sm-4' : $col = 'col-sm-8';
    // item classes
    $item['classes'] = $base_class . $col . ' ' . $item['type'];

    // There are 2 types of media items, links and videos

    // if this is a link,
    if ( $item['type'] === 'link' ) {
      // see if it is an internal or external link
      // Internal link
      if ( get_sub_field( 'home_media_link_type', $id ) === 'internal' ) {
        $link   = get_sub_field( 'home_media_button_link_internal', $id );
        $target = "_self";
        // external link
      } else {
        $link   = get_sub_field( 'home_media_button_link_external', $id );
        $target = "_blank";
      }
      // set up the button markup
      $button = sprintf( '<a href="%1$s" class="btn btn-default" target="%2$s">%3$s</a>', $link, $target, $buttonText );

    } else {

      // this is a video
      // create a unique ID for the modal
      // todo-jimmy : Tie modal ID to something other than a simple counter.
      // Ideally modal ID should be tied to something other than a counter, with the counter 2 class instances on a page
      // will result in ID collision. Can probably grab an ID from the ACF repeater instance, or something. Come back to
      // this if there's time at the end of the project.
      $modalID = 'media-' . $count;
      // data target for opening the modal
      $dataTarget = "#" . $modalID;
      // the video markup that we generate
      $video = Extras\videoLink( 'home_media_video_url', true );
      // instantiate a new modal (this class will output modal markup at the bottom of the page)
      // the class accepts 2 parameters, the video to play and a unique ID
      new Modal( $video, $modalID );
      // setup the button markup
      $button = sprintf( '<a href="#" class="btn btn-default play-btn" data-toggle="modal" data-target="%1$s">%2$s <i class="fa fa-play-circle-o" aria-hidden="true"></i></a>', $dataTarget, $buttonText );
    }

    // store the button markup
    $item['button'] = $button;
    // add the item to the items array
    $items[] = $item;
    // increment the counter
    $count ++;
    // end while have rows
  endwhile;
  // assign items to output
  $output = $items;

  // return the output
  return $output;
}

/**
 * Product Slider
 *
 * @param $field - ACF field
 *
 * @return array - array of slider data
 */
function productSlider( $field ) {

  $slides = $slide = [];
  $count  = 0;

  while ( have_rows( $field ) ) : the_row();
    $count ++;

    // grab the image array so we can return the thumbnail and the
    // main image
    $image = get_sub_field( 'product_image' );

    // vars
    $slide['url'] = $image['url'];
    $slide['alt'] = $image['alt'];
    $slide['id']  = 'slide-' . $count;
    // thumbnail
    $size              = 'thumbnail';
    $slide['thumb']    = $image['sizes'][ $size ];
    $slide['thumb-id'] = 'thumb-' . $count;
    $slide['data']     = false;
    $slide['class']    = ' product-image';

    // check to see if this is a video
    if ( get_sub_field( 'product_video' ) ) {

      // we have a video!
      $modalID = 'media-' . $count;
      // data target for opening the modal
      $dataTarget = "#" . $modalID;
      // the video markup that we generate
      $slide['data']  = ' data-toggle="modal" data-target="' . $dataTarget . ' "';
      $slide['class'] = ' product-video';
      $video          = Extras\videoLink( 'product_video_url', true );
      // instantiate a new modal (this class will output modal markup at the bottom of the page)
      // the class accepts 2 parameters, the video to play and a unique ID
      new Modal( $video, $modalID );

    }

    $slides[] = $slide;

  endwhile;

  return $slides;
}

/**
 * Product Branding
 *
 * @return array - product specific branding info
 */
function productBranding() {

  $branding = [];

  // get the product category (returns a term object)
  $productCategory = get_field( 'select_product_category' );

  $branding['productCategory'] = $productCategory->name;

  // add product name to body classes
  $body_class = 'product-category-' . $branding['productCategory'];


  //$branding['categoryColor']   = get_field( 'product_category_color', $productCategory );
  $branding['categoryColor'] = null;
  // multi select field
  $benefitsField        = get_field( 'benefits_and_features' );
  $benefits             = [];
  $branding['benefits'] = [];
  if ( ! empty( $benefitsField ) ) {
    foreach ( $benefitsField as $benefitTerm ) {
      $benefit['name']    = $benefitTerm->name;
      $benefitIcon        = get_field( 'benefits_features_icon', $benefitTerm );
      $benefit['iconURL'] = $benefitIcon['url'];
      $benefit['iconALT'] = $benefitIcon['alt'];
      $benefits[]         = $benefit;
    }
  }
  $branding['benefits'] = $benefits;

  return $branding;
}

function brand_body_class( $classes ) {
  if ( is_singular( 'products' ) ) {

    // get the product category (returns a term object)
    $term_object   = get_field( 'select_product_category' );
    $category_slug = $term_object->slug;

    // add product name to body classes
    $body_class = $category_slug;

    $classes[] = $body_class;
  }

  return $classes;
}

add_filter( 'body_class', __NAMESPACE__ . '\\brand_body_class' );

/**
 * Power Reviews
 *
 * @return array
 */
function powerReviews() {

  $review = [];

  $review['group_id']        = get_theme_mod( 'pr_merchant_id' );
  $review['site_id']         = get_field( 'pr_site_id' );
  $review['page_id']         = get_field( 'pr_page_id' );
  $review['page_id_variant'] = get_field( 'pr_page_id_variant' );
  $review['source']          = get_field( 'pr_source' );
  $review['styles']          = get_field( 'pr_style_stylesheet' );
  $review['write_review']    = get_field( 'pr_write_review' );

  return $review;
}


/**
 * Output nav images for product categories
 */
function navImages() {
  // get main nav items
  $elements = wp_get_nav_menu_items( 'main' );

  $styles = [];
  // go through them
  $menu_id = false;

  foreach ( $elements as $element ) {
    // get ID of product parent (class added via admin)
    if(in_array('products-nav', $element->classes ))
      $menu_id = $element->ID;

    // if parent is products
    if ( $element->menu_item_parent === strval($menu_id) ) {
      // slug - redundant
      $productSlug = preg_replace( '/\s+/', '-', strtolower( $element->title ) );
      // page id
      $productID = $element->object_id;
      // class for inline styles
      $productClass = 'menu-' . $productSlug;
      // get ACF field info
      $termID = get_field( 'select_category', $productID );
      // get the image array, and the image URL
      $productImage = get_field( 'product_category_image', 'term_' . $termID );
      $url          = $productImage['url'];
      // generate inline style markup
      $styles[] = sprintf( 'header .dropdown li.%1$s a::before {background:url(%2$s) no-repeat center;}', $productClass, $url );

    } // end if child of products
  } // end foreach
  // build the output
  ob_start(); ?>

  <style id="nav-images" type="text/css">
    <?php foreach ($styles as $style) : ?>
    <?= $style; ?>
    <?php endforeach; ?>
  </style>
  <?php $output = ob_get_clean();
  // echo the output
  echo $output;
}

add_action( 'wp_head', __NAMESPACE__ . '\\navImages' );

/**
 * Returns a list of products and associated data sheets,
 * Sorted by category name
 *
 * @return array
 */
function technicalData() {

  /*
   * 1) Get all terms from product categories taxonomy
   * 2) Loop through the terms, and for each term -
   *   2a) Get associated products
   * 3) Return all the things
   */


  // holds category names
  $categories = [];

  // loop through product categories
  while ( have_rows( 'tdb_add_categories' ) ) : the_row();

    // holds this category (term)
    $category = [];

    $term = get_sub_field( 'tdb_add_category' );

    // get the name of this category
    $category_name = $term->name;
    // get the ID of this category (use this to build the query)
    $category_id = $term->term_id;

    // add the name to the category array
    $category['name'] = $category_name;

    // get the product category color
    $category['category_color'] = get_field( 'product_category_color', $term );

    // get the products attached to this category
    // holds the products
    $products = [];

    $args = [
      'post_type'      => 'products',
      'posts_per_page' => - 1,
      'tax_query'      => [
        [
          'taxonomy' => 'product_categories',
          'field'    => 'id',
          'terms'    => $category_id
        ]
      ]
    ];
    // query
    $query = new \WP_Query( $args );
    if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post();

      // holds the product
      $product = [];

      $product_title = get_the_title();
      $product_link  = get_the_permalink();

      $product['product_title'] = $product_title;
      $product['link']          = $product_link;

      // skus
      $skus = [];
      if ( have_rows( 'sku_table' ) ) : while ( have_rows( 'sku_table' ) ) : the_row();
        $sku    = get_sub_field( 'product_sku' );
        $skus[] = $sku;
      endwhile; endif;

      $sku_list = implode( ', ', $skus );

      $product['sku_list'] = $sku_list;

      // holds skus, data sheets and bulletins for this product
      $dataSheets    = [];
      $dataBulletins = [];

      // get data sheets
      if ( have_rows( 'product_sds' ) ) : while ( have_rows( 'product_sds' ) ) :
        the_row();

        $dataSheet['title'] = get_sub_field( 'sds_title' );
        $dataSheet['file']  = get_sub_field( 'sds_attachment' );

        $dataSheets[] = $dataSheet;

      endwhile; endif;

      $product['data_sheets'] = $dataSheets;

      // get data bulletins
      if ( have_rows( 'product_tdb' ) ) : while ( have_rows( 'product_tdb' ) ) :
        the_row();

        $dataBulletin['title'] = get_sub_field( 'tdb_title' );
        $dataBulletin['file']  = get_sub_field( 'tdb_attachment' );

        $dataBulletins[] = $dataBulletin;

      endwhile; endif;

      $product['data_bulletins'] = $dataBulletins;


      $products[] = $product;

      // end if have posts
    endwhile;
      wp_reset_postdata(); endif;

    // add the products to the category
    $category['products'] = $products;
    // add this category to the categories array
    $categories[] = $category;

  endwhile; // end foreach terms as term

  return $categories;
}

/**
 * FAQ Data
 *
 * @return array
 */
function faq_sections() {
  // empty array for sections
  $sections = [];

  // Each FAQ section consists of side bar tabbed navigation
  // under which multiple sets of FAQs are grouped.

  /*
   * ****** General Section
   */

  // get the title for general faqs
  $general_title = get_field( 'faq_general_title' );
  // menu slug for general faqs (for navs)
  $general_slug = str_replace( ' ', '-', strtolower( $general_title ) );
  // holds panel groups
  $panel_groups = [];
  // run through repeater for gen cats
  if ( have_rows( 'add_general_faq_categories' ) ): while ( have_rows( 'add_general_faq_categories' ) ): the_row();


    // set the panel group title
    $panel_group['title'] = get_sub_field( 'general_faq_category_title' );
    $panel_group_id       = str_replace( ' ', '-', strtolower( $panel_group['title'] ) );

    // generate the panel group (the actual FAQs)
    $panels                = panel_group( 'add_general_faqs', 'general_faq_question', 'general_faq_answer', $panel_group_id );
    $panel_group['panels'] = $panels;

    $panel_groups[] = $panel_group;

  endwhile; endif; // end gen faq repeater

  // add everything to the section
  $section = [
    'title'        => $general_title,
    'slug'         => $general_slug,
    'classes'      => 'active',
    'panel_groups' => $panel_groups
  ];

  // add this section to sections
  $sections[] = $section;

  /*
   * ****** Products
   */
  if ( have_rows( 'faq_product_categories' ) ) :
    while ( have_rows( 'faq_product_categories' ) ) : the_row();
      // reset containers
      $section      = [];
      $panel_groups = [];

      // Set up section
      // get term object
      $term_object = get_sub_field( 'faq_product_category' );
      // get term ID
      $term_id          = $term_object->term_id;
      $section['title'] = $term_object->name;
      // add section slug
      $section['slug'] = $term_object->slug;
      // add additional classes
      $section['classes'] = '';

      // get products for this section
      $args = [
        'post_type'      => 'products',
        'posts_per_page' => - 1,
        'tax_query'      => [
          [
            'taxonomy' => 'product_categories',
            'field'    => 'id',
            'terms'    => $term_id
          ]
        ]
      ];

      $query = new \WP_Query( $args );
      if ( $query->have_posts() ):

        while ( $query->have_posts() ) : $query->the_post();

          $panel_group['title'] = get_the_title();
          // generate the panel group (the actual FAQs)
          // generate a unique ID for each panel group.
          $panel_group_id        = 'panel-group-' . get_the_ID();
          $panels                = panel_group( 'add_product_faqs', 'faq_product_question', 'faq_product_answer', $panel_group_id );
          $panel_group['panels'] = $panels;
          $panel_groups[]        = $panel_group;
          // end while have posts
          wp_reset_postdata(); endwhile;
        // end if have posts
      endif;
      $section['panel_groups'] = $panel_groups;
      $sections[]              = $section;
      // end while product categories
    endwhile;
    // end if product categories (repeater)
  endif;


  return $sections;
}


/**
 * Generates an FAQ panel group
 *
 * @param $repeater
 * @param $question
 * @param $answer
 * @param $id
 *
 * @return array
 */
function panel_group( $repeater, $question, $answer, $id ) {

  $count  = 0;
  $panels = [];

  if ( ! have_rows( $repeater ) ) {
    $panels = false;
  }

  while ( have_rows( $repeater ) ) : the_row();
    $count ++;
    $panel               = [];
    $panel['heading']    = get_sub_field( $question );
    $panel['content']    = get_sub_field( $answer );
    $panel['ID']         = $id . '-heading-' . $count;
    $panel['collapseID'] = $id . '-collapse-' . $count;
    // add this panel to panels
    $panels[] = $panel;
  endwhile;

  return $panels;

}

function where_to_buy( $field, $sub_field ) {
  // array for product categories
  $categories = [];

  // get list of categories
  while ( have_rows( $field ) ) : the_row();
    // array for this category
    $category = [];
    // get term info
    $term_object = get_sub_field( $sub_field );
    // get term ID
    $term_id           = $term_object->term_id;
    $term_slug         = $term_object->slug;
    $category['title'] = $term_object->name;
    // add section slug
    $category['slug']           = $term_object->slug;
    $image_object               = get_field( 'product_category_image', $term_object );
    $category['image_url']      = $image_object['url'];
    $category['image_alt']      = $image_object['alt'];
    $category['category_color'] = get_field( 'product_category_color', $term_object );

    // key value pair of slug => hex color
    $color[ $term_slug ] = $category['category_color'];

    // add additional classes
    $category['classes'] = $category['slug'];

    // get products for this category
    $args = [
      'post_type'      => 'products',
      'posts_per_page' => - 1,
      'tax_query'      => [
        [
          'taxonomy' => 'product_categories',
          'field'    => 'id',
          'terms'    => $term_id
        ]
      ]
    ];

    $query = new \WP_Query( $args );
    if ( $query->have_posts() ):

      // array for products attached to this category
      $products = [];

      while ( $query->have_posts() ) : $query->the_post();
        // array for this product
        $product          = [];
        $product['title'] = get_the_title();
        $product['slug']  = str_replace( [ "dynagrip", "adhesive", " ", "," ], [
          "",
          "",
          "-",
          ""
        ], strtolower( $product['title'] ) );

        // get retailers (attached to SKU table)
        $skus = [];
        if ( have_rows( 'sku_table' ) ) :
          while ( have_rows( 'sku_table' ) ) : the_row();
            $sku['number']         = get_sub_field( 'product_sku' );
            $sku_object            = get_sub_field( 'sku_size' );
            $sku['size']           = $sku_object->name;
            $single_image          = get_sub_field( 'individual_product_image' );
            $sku['single_img_url'] = $single_image['url'];
            $sku['single_img_alt'] = $single_image['alt'];
            $sku['retailers']      = ProductTabsController\product_retailers();
            $skus[]                = $sku;
          endwhile;
        endif;

        $product['skus'] = $skus;

        $products[] = $product;

      endwhile;
      wp_reset_postdata();

      $category['products'] = $products;

    endif;

    $categories[] = $category;

  endwhile;

  return $categories;
}

function color_styles() {

  $terms  = get_terms( 'product_categories' );
  $styles = [];

  foreach ( $terms as $term ) {

    // get term ID
    $term_slug  = $term->slug;
    $term_color = get_field( 'product_category_color', $term );
    // border colors
    $styles[] = sprintf( '
    .%1$s .wtb-category-button,
    .%1$s #where-to-buy .sku-nav-tab.active a,
    #%1$s .sku-nav-tab.active a {
      border-color: %2$s;
    }', $term_slug, $term_color );
    // background colors
    $styles[] = sprintf( '
    .%1$s.single .product-header,
    .%1$s .wtb-category-button h3,
    #%1$s .panel-footer,
    #%1$s .sku-retailer a:hover .buy,
    #%1$s .sku-retailer a:active .buy,
    .%1$s.single #where-to-buy .sku-retailer a:hover .buy,
    .%1$s.single #where-to-buy .sku-retailer a:active .buy,
    .page-template-template-faqs .faq-nav #faq-%1$s.active a,
    .page-template-template-faqs .faq-tab-content #%1$s h4::before,
    #%1$s .panel-toggle::before {
      background-color: %2$s;
    }', $term_slug, $term_color );
    // text colors
    $styles[] = sprintf( '
    .%1$s .wtb-category-button::after,
    #%1$s .panel-footer a::before,
    #%1$s .panel-toggle:after,
    .%1$s #where-to-buy .sku-nav-tab.active a:before,
    #%1$s .sku-nav-tab.active a:before {
      color: %2$s;
    }', $term_slug, $term_color );
  }

// build the output
  ob_start(); ?>

  <style id="brand-colors" type="text/css">
    <?php foreach ($styles as $style) : ?>
    <?= $style; ?>
    <?php endforeach; ?>
  </style>
  <?php $output = ob_get_clean();
// echo the output
  echo $output;

}

add_action( 'wp_head', __NAMESPACE__ . '\\color_styles' );
