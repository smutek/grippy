<?php

namespace Roots\Sage\Extras;

use Roots\Sage\Setup;

/**
 * Add <body> classes
 */
function body_class( $classes ) {
  // Add page slug if it doesn't exist
  if ( is_single() || is_page() && ! is_front_page() ) {
    if ( ! in_array( basename( get_permalink() ), $classes ) ) {
      $classes[] = basename( get_permalink() );
    }
  }

  // Add class if sidebar is active
  if ( Setup\display_sidebar() ) {
    $classes[] = 'sidebar-primary';
  }

  return $classes;
}

add_filter( 'body_class', __NAMESPACE__ . '\\body_class' );

/**
 * Clean up the_excerpt()
 */
function excerpt_more() {
  return ' &hellip; <a href="' . get_permalink() . '">' . __( 'Continued', 'sage' ) . '</a>';
}

add_filter( 'excerpt_more', __NAMESPACE__ . '\\excerpt_more' );

/**
 * Admin Styles
 *
 * Helps to visually separate ACF blocks out when there's a lot of them.
 */
function admin_styles() {
  echo '<style>
    .acf-postbox h2.hndle {
      background: #ebebeb;
      color: #424242;
      font-size: 21px !important;
    }
    .acf-postbox.closed h2.hndle {
    background: #dbdbdb;
    }
  </style>';
}

add_action( 'admin_head', __NAMESPACE__ . '\\admin_styles' );

/**
 * Add the SVG Mime type to the uploader
 * @author Alain Schlesser (alain.schlesser@gmail.com)
 * @link https://gist.github.com/schlessera/1eed8503110fb3076e73
 *
 * @param  array $mimes list of mime types that are allowed by the
 * WordPress uploader
 *
 * @return array modified version of the $mimes array
 *
 * @see https://codex.wordpress.org/Plugin_API/Filter_Reference/upload_mimes
 * @see http://www.w3.org/TR/SVG/mimereg.html
 */
function as_svg_mime_type( $mimes ) {
  // add official SVG mime type definition to the array of allowed mime types
  $mimes['svg'] = 'image/svg+xml';

  // return the modified array
  return $mimes;
}

add_filter( 'upload_mimes', __NAMESPACE__ . '\\as_svg_mime_type' );

/**
 * Fix Gravity Form Tabindex Conflicts
 *
 * @see http://gravitywiz.com/fix-gravity-form-tabindex-conflicts/
 *
 * @param $tab_index
 * @param bool $form
 *
 * @return int
 */
function gform_tabindexer( $tab_index, $form = false ) {
  $starting_index = 1000; // if you need a higher tabindex, update this number
  if ( $form ) {
    add_filter( 'gform_tabindex_' . $form['id'], __NAMESPACE__ . '\\gform_tabindexer' );
  }

  return \GFCommon::$tab_index >= $starting_index ? \GFCommon::$tab_index : $starting_index;
}

add_filter( 'gform_tabindex', __NAMESPACE__ . '\\gform_tabindexer', 10, 2 );


/**
 * oEmbed Attributes
 *
 * Add parameters to oEmbed query string. Useful for
 * turning off related videos and such.
 *
 * Basic field use: $video = videoLink('your_field_name');
 * Add second param if in a repeater: $video - videoLink('your_subfield_name', true);
 *
 * @see https://www.advancedcustomfields.com/resources/oembed/
 *
 * @param $field
 * @param bool $repeater defaults to false / true if repeater
 *
 * @return mixed  embed HTML
 */
function videoLink( $field, $repeater = false ) {
  global $post;
  // get current post ID
  $id = $post->ID;
  if ( ! $repeater ) {
    // get the field
    $videoFrame = get_field( $field, $id );
  } else {
    // if we are in a repeater
    $videoFrame = get_sub_field( $field, $id );
  }
  // use preg_match to find iframe src
  preg_match( '/src="(.+?)"/', $videoFrame, $matches );
  $src = $matches[1];
  // add extra params to iframe src
  $params    = array(
    'rel' => 0
  );
  $new_src   = add_query_arg( $params, $src );
  $videoLink = str_replace( $src, $new_src, $videoFrame );

  return $videoLink;
}

/**
 * Base files for CPT's
 * Use base-$cptName.php for singles
 * Fall back to the regular stuff
 *
 * @param $templates
 *
 * @see https://roots.io/sage/docs/theme-wrapper/#filtering-the-wrapper-custom-post-types
 * @return mixed
 */
function sage_wrap_base_cpts( $templates ) {
  // Get the current post type
  $cpt = get_post_type();
  if ( $cpt ) {
    // Shift the template to the front of the array
    array_unshift( $templates, 'base-' . $cpt . '.php' );
  }

  // Return our modified array with base-$cpt.php at the front of the queue
  return $templates;
}

// Add our function to the sage/wrap_base filter
add_filter( 'sage/wrap_base', __NAMESPACE__ . '\\sage_wrap_base_cpts' );


/**
 * Redirect press single back to press page
 *
 * Press posts have no single view. Just an archive page with minimal content
 * and links to external articles
 *
 */
function redirectPress() {
  $queried_post_type = get_query_var( 'post_type' );
  $archive           = get_post_type_archive_link( 'press' );
  if ( is_single() && 'press' == $queried_post_type ) {
    wp_redirect( $archive, 301 );
    exit;
  }
}

add_action( 'template_redirect', __NAMESPACE__ . '\\redirectPress' );


/**
 * Load Power Reviews
 *
 * Loads the PowerReviews scripts in the head
 * Checks to see if user is on single product or product listing
 * template. Merchant ID set via customizer
 *
 */
function loadPowerReviews() {

  // bail if not on single product or product category page
  if ( ! is_singular( 'products' ) && ! is_page_template( 'template-categories.php' ) ) {
    return;
  }
  // get the merchant ID from customizer options
  $merchantID = get_theme_mod( 'pr_merchant_id' );

  $script = sprintf( '<script type="text/javascript"
            src="https://cdn.powerreviews.com/repos/%1$s/pr/pwr/engine/js/full.js"></script>', $merchantID );

  echo $script;

}

add_action( 'wp_head', __NAMESPACE__ . '\\loadPowerReviews' );

/**
 * Probably a phone
 *
 * Uses Mobile_Detect class to check user agent. Checks to
 * see if the sniffed device is a mobile device, but not a tablet.
 * If it is a mobile device, but not a tablet, it's probably
 * a phone.
 *
 * Returns true or false.
 *
 * Use sparingly because user agent sniffing is sketchy.
 *
 * @return bool
 */
function probablyAPhone() {
  // initiate the class
  $detect = new \Mobile_Detect();
  // catch all. Includes phones and tablets.
  $detect->isMobile() ? $mobile = true : $mobile = false;
  $detect->isTablet() ? $tablet = true : $tablet = false;
  $mobile && ! $tablet ? $probPhone = true : $probPhone = false;
  return $probPhone;
}


/**
 * ACF Admin Access Control
 *
 * Hide / Show the ACF menu.
 *
 * Hides the ACF menu via a radio button tucked away in customizer.
 * Out of sight, out of mind.
 *
 * @return bool
 */

function acf_admin_control() {

  get_theme_mod('acf_visibility') === 'show' ? $return = true : $return = false;

  return $return;
}
add_filter('acf/settings/show_admin', __NAMESPACE__ . '\\acf_admin_control');

/******* JetPack ******/
/*
 * Remove JetPack devicepx
 */
/**
 *
 */
function dequeue_devicepx() {
  if(class_exists('Jetpack')) {
    wp_dequeue_script( 'devicepx' );
  }
}
add_action( 'wp_enqueue_scripts', __NAMESPACE__ . '\\dequeue_devicepx', 20 );
/*
 * Remove JetPack Styles
 */
// First, make sure Jetpack doesn't concatenate all its CSS
/**
 *
 */
function jetpackImplode() {
  if(class_exists('Jetpack')) {
    add_filter( 'jetpack_implode_frontend_css', '__return_false' );
  }
}
add_action('wp', __NAMESPACE__ . '\\jetpackImplode');
// Then, remove each CSS file, one at a time
/**
 *
 */
function nuke_jetpack_css() {
  if(class_exists('Jetpack')) {
    wp_deregister_style( 'AtD_style' ); // After the Deadline
    wp_deregister_style( 'jetpack_likes' ); // Likes
    wp_deregister_style( 'jetpack_related-posts' ); //Related Posts
    wp_deregister_style( 'jetpack-carousel' ); // Carousel
    wp_deregister_style( 'grunion.css' ); // Grunion contact form
    wp_deregister_style( 'the-neverending-homepage' ); // Infinite Scroll
    wp_deregister_style( 'infinity-twentyten' ); // Infinite Scroll - Twentyten Theme
    wp_deregister_style( 'infinity-twentyeleven' ); // Infinite Scroll - Twentyeleven Theme
    wp_deregister_style( 'infinity-twentytwelve' ); // Infinite Scroll - Twentytwelve Theme
    wp_deregister_style( 'noticons' ); // Notes
    wp_deregister_style( 'post-by-email' ); // Post by Email
    wp_deregister_style( 'publicize' ); // Publicize
    wp_deregister_style( 'sharedaddy' ); // Sharedaddy
    wp_deregister_style( 'sharing' ); // Sharedaddy Sharing
    wp_deregister_style( 'stats_reports_css' ); // Stats
    wp_deregister_style( 'jetpack-widgets' ); // Widgets
    wp_deregister_style( 'jetpack-slideshow' ); // Slideshows
    wp_deregister_style( 'presentations' ); // Presentation shortcode
    wp_deregister_style( 'jetpack-subscriptions' ); // Subscriptions
    wp_deregister_style( 'tiled-gallery' ); // Tiled Galleries
    wp_deregister_style( 'widget-conditions' ); // Widget Visibility
    wp_deregister_style( 'jetpack_display_posts_widget' ); // Display Posts Widget
    wp_deregister_style( 'gravatar-profile-widget' ); // Gravatar Widget
    wp_deregister_style( 'widget-grid-and-list' ); // Top Posts widget
    wp_deregister_style( 'jetpack-widgets' ); // Widgets
  }
}
add_action( 'wp_print_styles', __NAMESPACE__ . '\\nuke_jetpack_css' );
