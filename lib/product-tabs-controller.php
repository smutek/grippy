<?php

namespace Roots\Sage\ProductTabsController;

/**
 * Product Info
 * Product specifics, for the product specs table
 *
 * Moved to its own file because it kept growing, like Ungoilant.
 *
 * @return array
 */
function productInfo() {

  $info    = [];
  $navTabs = [];

  $info['overview_show_tab'] = $info['specs_show_tab'] = $info['wtb_show_tab'] = $info['tips_show_tab'] = $info['skus_show_tab'] = $info['sds_show_tab'] = $info['reviews_show_tab'] = $info['additional_tabs'] = false;

  /*
   * ************************* Overview
   */
  if ( get_field( 'overview_show_tab' ) ) :

    $info['overview_show_tab'] = true;
    $title                     = get_field( 'overview_tab_title' );
    $id                        = strtolower( str_replace( ' ', '-', $title ) );
    $id                        = preg_replace( '/[^A-Za-z0-9\-]/', '', $id );

    $navTab = sprintf( '<a class="%1$s-nav-tab" href="#%1$s" aria-controls="%1$s" role="tab" data-toggle="tab">%2$s</a>', $id, $title );

    $navTabs[] = $navTab;

    $info['overview_tab_title'] = $title;
    $info['overview_tab_id']    = $id;


    $info['overview'] = get_field( 'overview_text' );
    $info['excerpt']  = get_field( 'product_excerpt' );
    $excerptImage     = get_field( 'product_excerpt_image' );
    $info['img_url']  = $excerptImage['url'];
    $info['img_alt']  = $excerptImage['alt'];

    $designations = [];

    if ( have_rows( 'ovverview_designations' ) ) : while ( have_rows( 'ovverview_designations' ) ) : the_row();

      $designation = get_sub_field( 'designation_image' );

      $designations[] = $designation;

    endwhile; endif;

    $info['designations'] = $designations;

  endif;

  /*
 * ************************* Specs
 */

  if ( get_field( 'specs_show_tab' ) ) :

    $info['specs_show_tab'] = true;

    $title = get_field( 'specs_tab_title' );
    $id    = strtolower( str_replace( ' ', '-', $title ) );
    $id    = preg_replace( '/[^A-Za-z0-9\-]/', '', $id );

    $navTab = sprintf( '<a class="%1$s-nav-tab" href="#%1$s" aria-controls="%1$s" role="tab" data-toggle="tab">%2$s</a>', $id, $title );

    $navTabs[] = $navTab;

    $title                   = get_field( 'specs_tab_title' );
    $info['specs_tab_title'] = $title;
    $info['specs_tab_id']    = $id;

    // product specs
    $info['rec_use']             = get_field( 'recommended_use' );
    $info['not_rec']             = get_field( 'not_recommended_for' );
    $info['manufacturer_rec']    = get_field( 'manufacturer_recommendations' );
    $info['adheres']             = get_field( 'adheres_to' );
    $info['physical']            = get_field( 'physical_properties' );
    $info['coverage']            = get_field( 'coverage' );
    $info['app_temperature']     = get_field( 'application_temperature' );
    $info['service_temperature'] = get_field( 'service_temperature' );
    $info['specs']               = get_field( 'specifications' );
    // colors available get built in the SKU's section
    $info['colors_available'] = [];

  endif;

  /*
 * ************************* Where To Buy
 */

  if ( get_field( 'wtb_show_tab' ) ) :

    // where to buy
    $info['wtb_show_tab'] = true;

    $title = get_field( 'wtb_tab_title' );
    // title can be dynamic but slug needs to remain where-to-buy
    // This slug is hard coded in a few places and the hover colors
    // on product single WTB and the WTB jump to link will both stop
    // working if this slug changes.
    // Comment out dynamic slug generation
    //$id    = strtolower( str_replace( ' ', '-', $title ) );
    //$id    = preg_replace( '/[^A-Za-z0-9\-]/', '', $id );
    $id    = "where-to-buy";

    $navTab = sprintf( '<a class="%1$s-nav-tab" href="#%1$s" aria-controls="%1$s" role="tab" data-toggle="tab">%2$s</a>', $id, $title );

    $navTabs[] = $navTab;

    $title                 = get_field( 'wtb_tab_title' );
    $info['wtb_tab_title'] = $title;
    $info['wtb_tab_id']    = $id;

  endif;

  /*
 * ************************* Tips & Tricks
 */

  if ( get_field( 'tips_show_tab' ) ) :

    // tips and tricks
    $info['tips_show_tab'] = true;

    $title = get_field( 'tips_tab_title' );
    $id    = strtolower( str_replace( ' ', '-', $title ) );
    $id    = preg_replace( '/[^A-Za-z0-9\-]/', '', $id );

    $navTab = sprintf( '<a class="%1$s-nav-tab" href="#%1$s" aria-controls="%1$s" role="tab" data-toggle="tab">%2$s</a>', $id, $title );

    $navTabs[] = $navTab;

    $title                  = get_field( 'tips_tab_title' );
    $info['tips_tab_title'] = $title;
    $info['tips_tab_id']    = $id;

  endif;

  /*
 * ************************* SKUs
 */

  if ( get_field( 'skus_show_tab' ) ) :
    // SKU table

    $rows = [];
    $skus = [];

    $info['skus_show_tab'] = true;

    $title = get_field( 'skus_tab_title' );
    $id    = strtolower( str_replace( ' ', '-', $title ) );
    $id    = preg_replace( '/[^A-Za-z0-9\-]/', '', $id );

    $navTab = sprintf( '<a class="%1$s-nav-tab" href="#%1$s" aria-controls="%1$s" role="tab" data-toggle="tab">%2$s</a>', $id, $title );

    $navTabs[] = $navTab;

    $title                  = get_field( 'skus_tab_title' );
    $info['skus_tab_title'] = $title;
    $info['skus_tab_id']    = $id;

    if ( have_rows( 'sku_table' ) ) : while ( have_rows( 'sku_table' ) ) : the_row();

      $row['sku'] = get_sub_field( 'product_sku' );

      // get the sku size (returns a term object)
      $size = get_sub_field( 'sku_size' );
      $row['size']       = $size->name;
      $row['pack']       = get_sub_field( 'pack' );
      $row['weight']     = get_sub_field( 'weight' );
      $row['dimensions'] = get_sub_field( 'dimensions' );
      $row['volume']     = get_sub_field( 'volume' );
      $row['pallet']     = get_sub_field( 'per_pallet' );
      $single_image      = get_sub_field( 'individual_product_image' );
      $row['single_url'] = $single_image['url'];
      $row['single_alt'] = $single_image['alt'];

      // get available colors (shows up in specs tab)
      $color_terms = get_sub_field('available_colors');

      $colors_available = [];

      foreach ( $color_terms as $color_term ) {
        $color = [];
        $color['name'] = $color_term->name;
        $color['hex'] = get_field( 'color_field', $color_term );
        $colors_available[] = $color;
      }
      $info['colors_available'] = $colors_available;
      // get the retailers attached to this SKU
      //$retailers = [];

      $retailers = product_retailers();

      $row['retailers'] = $retailers;

      $rows[] = $row;
      $skus[] = $row['sku'];

    endwhile; endif;

    $info['table_rows'] = $rows;

    $sku_content = get_field( 'sku_lower_content' );

    $sku_content !== "" ? $info['sku_lower'] = $sku_content : $info['sku_lower'] = false;

  endif;

  /*
 * ************************* SDS / TDB
 */

  if ( get_field( 'sds_show_tab' ) ) :

    // sds / tdb

    $info['sds_show_tab'] = true;


    $title = get_field( 'sds_tab_title' );
    $id    = strtolower( str_replace( ' ', '-', $title ) );
    $id    = preg_replace( '/[^A-Za-z0-9\-]/', '', $id );

    $navTab = sprintf( '<a class="%1$s-nav-tab" href="#%1$s" aria-controls="%1$s" role="tab" data-toggle="tab">%2$s</a>', $id, $title );

    $navTabs[] = $navTab;

    $title                 = get_field( 'sds_tab_title' );
    $info['sds_tab_title'] = $title;
    $info['sds_tab_id']    = $id;

    // generate a comma separated string listing SKU numbers
    $skuList          = rtrim( implode( ', ', $skus ), ',' );
    $info['sku_list'] = $skuList;

    $dataSheets    = [];
    $dataBulletins = [];

    if ( have_rows( 'product_sds' ) ) : while ( have_rows( 'product_sds' ) ) :
      the_row();

      $dataSheet['title'] = get_sub_field( 'sds_title' );
      $dataSheet['file']  = get_sub_field( 'sds_attachment' );

      $dataSheets[] = $dataSheet;

    endwhile; endif;

    $info['data_sheets'] = $dataSheets;

    if ( have_rows( 'product_tdb' ) ) : while ( have_rows( 'product_tdb' ) ) :
      the_row();

      $dataBulletin['title'] = get_sub_field( 'tdb_title' );
      $dataBulletin['file']  = get_sub_field( 'tdb_attachment' );

      $dataBulletins[] = $dataBulletin;

    endwhile; endif;

    $info['data_bulletins'] = $dataBulletins;

  endif;

  /*
 * ************************* Reviews
 */


  if ( get_field( 'reviews_show_tab' ) ) :

    $info['reviews_show_tab'] = true;

    $title = get_field( 'reviews_tab_title' );
    $id    = strtolower( str_replace( ' ', '-', $title ) );
    $id    = preg_replace( '/[^A-Za-z0-9\-]/', '', $id );

    $navTab = sprintf( '<a class="%1$s-nav-tab" href="#%1$s" aria-controls="%1$s" role="tab" data-toggle="tab">%2$s</a>', $id, $title );

    $navTabs[] = $navTab;

    $title                     = get_field( 'reviews_tab_title' );
    $info['reviews_tab_title'] = $title;
    $info['reviews_tab_id']    = $id;

  endif;

  /*
 * ************************* Additional Tabs
 */

  if ( have_rows( 'additional_tabs' ) ) :

    $additional_tabs = [];

    while ( have_rows( 'additional_tabs' ) ) : the_row();

      $title = get_sub_field( 'additional_tab_title' );
      $id    = strtolower( str_replace( ' ', '-', $title ) );
      $id    = preg_replace( '/[^A-Za-z0-9\-]/', '', $id );

      $navTab = sprintf( '<a class="%1$s-nav-tab" href="#%1$s" aria-controls="%1$s" role="tab" data-toggle="tab">%2$s</a>', $id, $title );

      $navTabs[] = $navTab;

      $additional_tab['id']      = $id;
      $additional_tab['title']   = $title;
      $additional_tab['content'] = get_sub_field( 'additional_tab_content' );

      $additional_tabs[] = $additional_tab;

    endwhile;

    $info['additional_tabs'] = $additional_tabs;

  endif;

  $info['navTabs'] = $navTabs;

  return $info;

}

/**
 * @return array|bool
 */
function product_retailers() {

  $retailers = false;

  if ( have_rows( 'retailers' ) ) : while ( have_rows( 'retailers' ) ) : the_row();
    // holds the retailer info
    $retailer         = [];
    // returns the ID of a custom post type (a retailer)
    $retailer_id      = get_sub_field( 'product_retailer' );
    $retailer['id']   = $retailer_id;
    // product specific URL saved with the product SKU
    $retailer_url = get_sub_field('url_override');

    if(! $retailer_url) {
      // if retailer url is blank, get the default url from the custom post type
      $retailer_url = get_field( 'retailer_url', $retailer_id );
    }
    // logo field is stored with the retailer custom post type
    $retailer_logo = get_field( 'retailer_logo', $retailer_id );

    $retailer['logo_url'] = $retailer_logo['url'];
    $retailer['logo_alt'] = $retailer_logo['alt'];

    $retailer['url'] = $retailer_url;

    $retailer['name'] = get_the_title( $retailer_id );

    $retailers[] = $retailer;

  endwhile; endif;


  return $retailers;

}
