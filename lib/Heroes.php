<?php
namespace Roots\Sage\Heroes;

use Roots\Sage\Wrapper\SageWrapping;
use Roots\Sage\Modals\Modal;
use Roots\Sage\Extras;

/**
 * Class Heroes
 * @package Roots\Sage\Heros
 */
class Hero {

  /**
   * The post ID
   * @var
   */
  public $postID;

  /**
   * Heroes constructor.
   *
   * @param $postID
   */
  public function __construct( $postID ) {
    // store the ID as a property of the object instance
    $this->postID = $postID;

    add_action( 'get_hero', [ $this, 'outputHero' ] );

  }

  /**
   * File path to the hero template
   *
   * @return SageWrapping
   */
  public function filePath() {
    $path     = 'templates/modules/hero.php';
    $template = new SageWrapping( $path );
    return $template;
  }

  /**
   * Assemble the content for the hero
   *
   * @return mixed
   */
  public function heroContent() {

    $postID = $this->postID;

    $hero['heading'] = get_field( 'hero_heading', $postID );
    $hero['copy']    = get_field( 'hero_copy', $postID );
    $hero['button']  = $this->heroButton();
    $hero['bg']      = get_field( 'hero_bg', $postID );
    $hero['videoBG'] = false;
    $hero['textBG']  = false;

    if ( get_field( 'hero_use_background_video', $postID ) && !Extras\probablyAPhone())
      $hero['videoBG'] = get_field( 'hero_background_video_link', $postID );

    if( get_field( 'use_image_for_heading', $postID ) && get_field( 'hero_heading_copy_image', $postID )) {
      $image = get_field( 'hero_heading_copy_image', $postID );
      $hero['textBG'] = $image['url'];
    }

    return $hero;

  }

  /**
   * Returns markup for the hero CTA button
   *
   * @return null|string
   */
  public function heroButton() {

    $postID     = $this->postID;
    $button     = false;

    if(get_field('hero_hide_cta_button', $postID))
      return $button;

    $buttonType = get_field( 'hero_button_type', $postID );
    $buttonText = get_field( 'hero_button_text', $postID );

    // link button type
    if ( $buttonType === 'link' ) {

      if ( get_field( 'hero_link_type', $postID ) === 'internal' ) {
        // internal link
        $link   = get_field( 'hero_button_link_internal', $postID );
        $target = "_self";

      } else {
        // external link
        $link   = get_field( 'hero_button_link_external', $postID );
        $target = "_blank";
      }

      $button = sprintf( '<a href="%1$s" class="btn btn-default" target="%2$s">%3$s</a>', $link, $target, $buttonText );

    } elseif ( $buttonType === 'video' ) {

      $heroID     = $this->heroID();
      $modalID    = 'media-' . $heroID;
      $dataTarget = '#' . $modalID;
      $video      = Extras\videoLink( 'hero_video_url' );

      new Modal( $video, $modalID );

      $button = sprintf( '<a href="#" class="btn btn-default play-btn" data-toggle="modal" data-target="%1$s">%2$s <i class="fa fa-play-circle-o" aria-hidden="true"></i></a>', $dataTarget, $buttonText );

    }

    return $button;
  }

  /**
   * Heroes ID
   *
   * Generate a somewhat unique ID to use when calling a modal
   *
   * @return string
   */
  public function heroID() {

    $postID = $this->postID;
    $title  = str_replace( ' ', '-', get_the_title( $postID ) );
    $heroID = $postID . '-' . $title;

    return $heroID;
  }

  public function renderHero() {
    ob_start();
    include $this->filePath();
    $output = ob_get_clean();

    return $output;
  }

  public function outputHero() {
    $hero = $this->renderHero();
    echo $hero;
  }

}
