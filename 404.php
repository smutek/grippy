<?php get_template_part('templates/page', 'header'); ?>

<div class="alert alert-warning">
  <?php _e('<h2>404</h2>', 'sage'); ?>
  <?php _e('<p>Sorry, but the page you were trying to view does not exist.</p>', 'sage'); ?>
</div>

